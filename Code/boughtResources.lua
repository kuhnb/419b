--boughtResources.lua
--this file will be responsible for accounting for the resources that a user has bought
--------------------------------------------------
local composer = require( "composer" )
local myData = require("Code.mydata")
--------------------------------------------------
--NAME OF THE GRAPHICS FILE
local bR_nameOfFile="inventory.txt"

--Default Resources (Default GFX/SFX are in [1] and [4])
local default = {true,false,false,true,false,false}



--this function will return
function getResourcesArray()
  local result = {}

  --First, check if file exists
	local inventoryExists =  boughtResources_doesFileExist(bR_nameOfFile, system.DocumentsDirectory)
  --print("inventoryExists:"..tostring(inventoryExists))

	if inventoryExists then
    print("Inventory Exists.")
    --it exists, read it
    result = boughtResources_readFile(bR_nameOfFile,system.DocumentsDirectory)
	else
    --write the file with the default path
    print("Inventory Does Not Exist.\nCreating Default Inventory File.")
    boughtResources_writeFile(default)
    --then use the default resource array
    result=default
  end

  --return the graphicsPath
  return result
end

--function to check if graphics file exists
function boughtResources_doesFileExist( fname, path )
    local result = false
    -- Path for the file
    local filePath = system.pathForFile( fname, path )
    if ( filePath ) then
        local file, errorString = io.open( filePath, "r" ) --read
        if file then
          result=true
          file:close()
        end
    end
    --return boolean
    return result
end

--function to read graphics file
function boughtResources_readFile(fname, path)
  local result = {}
  local filePath = system.pathForFile( fname, path )
    if ( filePath ) then
        local file, errorString = io.open( filePath, "r" ) --read

        if not file then
            -- Error occurred; output the cause
            print( "Inventory File Read Error.")
        else
            -- File exists! Read from file
            if fname == bR_nameOfFile then

              --ready to read file
              local contents = file:read("*l")

              --while loop to read all of the components of one level
              local i=1
              while contents~=nil do

                if tostring(contents)=="true" then
                  --true
                  result[i]=true
                else
                  --false
                  result[i]=false
                end
                --Lastly, addvance the file read for loop condition
                contents = file:read("*l")
                --and increment i
                i=i+1
              end
              --set myData appropriately
              updateGraphicsAndAudioBought(result)
            end

        end
        -- Close the file handle
        file:close()
    end
  --return result
  return result

end

--function to write new graphics path to graphics file.
function boughtResources_writeFile(booleanArray)
  print(bR_nameOfFile.." "..tostring(system.DocumentsDirectory))
  local path = system.pathForFile(bR_nameOfFile, system.DocumentsDirectory)
  local file, errorString = io.open( path,  "w+") --All existing data is removed if file exists or new file is created with read write permissions.
  if file then
    --Error not handled
    --print("File error: " .. errorString)

    for i=1,(table.getn(booleanArray)) do
      local bool = tostring(booleanArray[i])
      print("bool"..i..": "..bool) --test print
      if(i ~= (table.getn(booleanArray))) then
        file:write(bool.."\n")
      else
        file:write(bool)
      end
    end
    io.close(file)
  else
    print("File Error String: "..tostring(errorString))
  end
  file = nil

  --read after write, to update myData
  boughtResources_readFile(bR_nameOfFile,system.DocumentsDirectory)

end

function writeBought()
  local booleanArray = {}
  --first 3 are for graphics
  booleanArray[1] = myData.graphicsBought[1]
  booleanArray[2] = myData.graphicsBought[2]
  booleanArray[3] = myData.graphicsBought[3]
  --last 3 are for audio
  booleanArray[4] = myData.audioBought[1]
  booleanArray[5] = myData.audioBought[2]
  booleanArray[6] = myData.audioBought[3]

  boughtResources_writeFile(booleanArray)
end

function updateGraphicsAndAudioBought(arr)
  myData.graphicsBought={}
  myData.audioBought={}
  --first 3 are for graphics
  myData.graphicsBought[1]=arr[1]
  myData.graphicsBought[2]=arr[2]
  myData.graphicsBought[3]=arr[3]
  --last 3 are for audio
  myData.audioBought[1]=arr[4]
  myData.audioBought[2]=arr[5]
  myData.audioBought[3]=arr[6]
end
