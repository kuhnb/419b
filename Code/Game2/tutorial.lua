local composer = require( "composer" )
local scene = composer.newScene()
local widget = require("widget")

---------------------------------------------------------------------------------

-- local forward references should go here
local background
local next
local back
local text
local backi
local soundEffect = audio.loadSound( "SFX/Common/Menu1.wav" )

---------------------------------------------------------------------------------
--Return to title.lua
function toTitle(event)
  --Generate options
  local options = {
    effect = "fade",
    time = 1000
  }
  --Go to title.lua
  if event.phase == "ended" then
    audio.play(soundEffect, {channel=2})
    composer.gotoScene("Code.Game2.title", options)
  end
end

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   --Generate and set background image.
   background = display.newImageRect("GFX/Common/title.png", 480, 320)
   background.anchorX = 0
   background.anchorY = 0
   background.x = 0
   background.y = 0
   sceneGroup:insert(background)

   --Create and set back button image.

    backButton = widget.newButton{
      left = display.contentWidth - 43,
      top = display.contentHeight - 43,
      shape = "roundedRect",
      font = native.systemFont,
      width = 40,
      height = 40,
      cornerRadius = 4,
      labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
      fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
      strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
      strokeWidth = 4,
      onEvent = toTitle
    }
    sceneGroup:insert(backButton)

    backi = display.newImageRect("GFX/Common/left224.png", 35, 35)
    backi.x = backButton.x
    backi.y = backButton.y
    sceneGroup:insert(backi)
   --Generate and set tutorial text.
   local options = {
     parent = sceneGroup,
     text = "Tutorial:\n\nCarefully read the problem at the top of the screen, then select the correct answer below it.",
     x = display.contentCenterX,
     y = display.contentCenterY,
     width = 340,
     font = native.systemFont,
     fontSize = 18,
     align = "center"
   }
   text = display.newText(options)
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
     --Set button listeners.
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
     --Remove button listeners to prevent bugs.
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view
   --Clean up. Maybe the code could use some Old Spice...
   background:removeSelf()
   background = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
