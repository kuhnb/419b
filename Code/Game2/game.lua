-- Code.GAme2.game.lua
-- Actual Game. Applied Boolean Logic Within Code Samples

local composer = require( "composer" )
local scene = composer.newScene()
local parse = require( "mod_parse" )
local myData = require("Code.mydata")
local cur = require("Code.currency")
--local scoreSave = require("Code.Leaderboard.scoreSaver")
require("Code.Leaderboard.scoreSaver")
local widget = require("widget")
---------------------------------------------------------------------------------

-- local forward references should go here
local background
local question
local text1
local text2
local text3
local buttonA
local buttonB
local buttonC
local buttonD
local levelNum
local level
local exit
local displayed = false
local alert
local errorCount
local startTime
local endTime
local pauseButton
local optionsButton
local opti
--For the score of each level, the value will be stored in levelScore, and calculated in function "calculateLevelScore()".
local levelScore
--The leveldifficulty should be: 0=Easy, 1=Medium, 2=Hard
local levelDifficulty

---------------------------------------------------------------------------------
--Return to the title screen.
local function toTitle(event)
  --Generate options.
  local options = {
    effect = "fade",
    time = 1000
  }
  --Go to title.lua.
  if event.phase == "ended" then
    audio.stop(1)
    composer.gotoScene("Code.Game2.title", options)
  end
end

function scene:optionsToTitle(event)
  --Generate appropriate options
  local options = {
    effect = "fade",
    time = 1000
  }
  --Go to title.lua
  if event.phase == "ended" then
    audio.stop(1)

    composer.gotoScene("Code.Game2.title", options)
    composer.hideOverlay("fade", 250)
  end
end

--Check to see if the player gave the right answers.
--TODO: optimize this.
local function correctAnswer (event)
  local attribute = "G2L" .. levelNum
    --Generate options and parameters.
    local nextNum = levelNum + 1
    local options = {
      effect = "fade",
      time = 500,
      params = {
        num = nextNum,
        level = "2-" .. nextNum .. ".png"
      }
    }
    if event.phase == "ended" then
      --Go ot the right scene according to what level it is.
      if levelNum ~= 6 then
        --Set the endTime
        endTime = system.getTimer()/1000 - startTime

        --CalculateLevelScore (which also sends the score to App42)
        calculateLevelScore()

        --Send appropriate data to parse
        if myData.consent == true then
          parse:updateObject("LevelTime", myData.ltid, {[attribute] = endTime})
          parse:updateObject("NumMistake", myData.nmid, {[attribute] = errorCount})
        end
        --Analyze combo data
        --ComboPlay - the number of games played in one sitting
        if myData.g2Combo then
          myData.g2Combo = myData.g2Combo + 1
        else
          myData.g2Combo = 1
        end
        --ComboCorrect - the number of levels the user correctly completed in a row
        if myData.g2ComboCorrect then
          if errorCount == 0 then
            myData.g2ComboCorrect = myData.g2ComboCorrect + 1
          else
            if myData.g2ComboCorrect > myData.g2RecordComboCorrect then
              myData.g2RecordComboCorrect = myData.g2ComboCorrect
            end
            myData.g2ComboCorrect = 0
          end
        else
          if errorCount == 0 then
            myData.g2ComboCorrect = 1
          else
            myData.g2ComboCorrect = 0
          end
        end

        composer.gotoScene("Code.Game2.nextLevel", options)
      else
        composer.gotoScene("Code.Game2.title", options)
      end
    end
end

--If the player got an answer wrong, we want to notify them and display a hint.
local function wrongAnswer (event)
  --Display an alert saying the user got the question wrong.
  if event.phase == "ended" then
    displayed = true
    native.showAlert("Incorrect", "Incorrect: (hint functionality to be added in future update)", {"OK =("})
    errorCount = errorCount + 1
  end
end

--Open options overlay
local function openOptions(event)
  --Generate options
  local options = {
    effect = "fade",
    --This ensures players cannot interact with menu while options are being displayed.
    isModal = true,
    time = 500,
    params = {
      text1 = "Resume"
    }
  }
  --Show the options overlay.
  if event.phase == "ended" then
    composer.showOverlay("Code.options", options)
  end
end

--Used to calculate the final "levelScore" value.
function calculateLevelScore()
 --levelScore = (Level Max Score)*(Difficulty Value) - (Seconds to Complete).
  levelScore = -1
 --HARDCODED:
  levelDifficulty = 0
 --Max Level Scores:
  local maxLevel1=120
  local maxLevel2=120
  local maxLevel3=120
  local maxLevel4=120
  local maxLevel5=120
 --Difficulty Modifiers:
  local easy=1
  local medium=1.5
  local hard=2
  --Name of Game
  local nameOfGame

  --If a valid level number
  if levelNum > 0 and levelNum < 6 then
    --Step 1) Set levelDifficulty
    if levelDifficulty == 0 then
      levelDifficulty = easy
     elseif levelDifficulty == 1 then
      levelDifficulty = medium
     elseif levelDifficulty == 2 then
      levelDifficulty = hard
     else
      print("Error Calculating Level Difficulty. (In: function calculateLevelScore())")
      print("levelDifficulty: " .. levelDifficulty)
    end
    --Step 2) Set levelScore = the current level * the difficulty modifier
    if levelNum == 1 then
      nameOfGame="21"
      levelScore = maxLevel1 * levelDifficulty
     elseif levelNum == 2 then
      nameOfGame="22"
      levelScore = maxLevel2 * levelDifficulty
     elseif levelNum == 3 then
      nameOfGame="23"
      levelScore = maxLevel3 * levelDifficulty
     elseif levelNum == 4 then
      nameOfGame="24"
      levelScore = maxLevel4 * levelDifficulty
     else
      nameOfGame="25"
      levelScore = maxLevel5 * levelDifficulty
    end
    --Step 3) Decrement levelScore by endTime
    if levelScore > 0 then
      levelScore = levelScore - endTime
      if levelScore < 0 then
        levelScore = 0
      end
    end
    --Step 4) Math.Floor value
    levelScore = math.floor(levelScore)
    --TEST: Print Level Score
    print("LEVEL "..levelNum.." SCORE: "..levelScore)

    --Step 5) Currency. Increment Player's Coins
    --//!@#come back here
    changeUserCoins(levelScore)

    --TODO: APP42 Stuff

    local usr = myData.App42Username
    --Step 5) Save Score on App42
    local globalCall = function(adding)
      print("global created "..adding)
      return function(score)
          if score == nil then
            score = 0
          end
          print("global called "..score)
          saveUserScore("global",usr,  adding + score)
          print("Hello"..score)
        end
    end

    local call = function(score)
      if score == nil then
        score = 0
      end
      if(score < levelScore) then
        saveUserScore(nameOfGame,usr,levelScore)
        getHighestScoresByUser("Global",usr,globalCall(levelScore - score))
      end
    end

    getHighestScoresByUser(nameOfGame,usr,call)

  else
    --0 > levelNum >= 6. Not currently supported. Show on console.
    print("Error Calculating Level Score. (In: function calculateLevelScore())")
    print("Level Number: " .. levelNum)
  end



end --end function calculateLevelScore()


-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   levelNum = event.params.num
   level = event.params.level
   errorCount = 0
   startTime = system.getTimer()/1000
   --Load background image and set in correct place
   background = display.newImageRect("GFX/Common/title.png", 480, 320)
   background.anchorX = 0
   background.anchorY = 0
   background.x = 0
   background.y = 0
   sceneGroup:insert(background)

   if levelNum ~= 6 then
     --This is level one
     local glevel = "Levels/Game2/" .. event.params.level
     question = display.newImageRect(glevel, 480, 275)
     question.anchorX = 0
     question.anchorY = 0
     question.x = 0
     question.y = 0
     sceneGroup:insert(question)

     --Create and set the a button image.
     buttonA = display.newImageRect("GFX/Game2/buttonA.png", 50, 50)
     buttonA.anchorX = 0
     buttonA.anchorY = 0
     buttonA.x = display.contentWidth/4 - 70
     buttonA.y = display.contentHeight - 52
     sceneGroup:insert(buttonA)

     --Generate and set the b button.
     buttonB = display.newImageRect("GFX/Game2/buttonB.png", 50, 50)
     buttonB.anchorX = 0
     buttonB.anchorY = 0
     buttonB.x = 2*display.contentWidth/4 - 70
     buttonB.y = display.contentHeight - 52
     sceneGroup:insert(buttonB)

     --Generate and set the c button
     buttonC = display.newImageRect("GFX/Game2/buttonC.png", 50, 50)
     buttonC.anchorX = 0
     buttonC.anchorY = 0
     buttonC.x = 3*display.contentWidth/4 - 70
     buttonC.y = display.contentHeight - 52
     sceneGroup:insert(buttonC)

     --Generate and set the d button
     buttonD = display.newImageRect("GFX/Game2/buttonD.png", 50, 50)
     buttonD.anchorX = 0
     buttonD.anchorY = 0
     buttonD.x = 4*display.contentWidth/4 - 70
     buttonD.y = display.contentHeight - 52
     sceneGroup:insert(buttonD)

   else
     text1 = display.newText(sceneGroup, "Drop the ba-- plot!", display.contentCenterX, 100, native.systemFont, 20)
     --Generate and set the exit button image.
     exit = display.newImageRect("GFX/Common/exit.png", 100, 25)
     exit.x = display.contentWidth - 50
     exit.y = display.contentHeight - 12
     sceneGroup:insert(exit)
   end

   --Create and set an options button
   optionsButton = widget.newButton{
     left = 0,
     top = display.contentHeight-43,
     shape = "roundedRect",
     font = native.systemFont,
     width = 40,
     height = 40,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = openOptions
   }
   sceneGroup:insert(optionsButton)

   opti = display.newImageRect("GFX/Common/pauseButton.png", 50, 50)
   opti.x = optionsButton.x
   opti.y = optionsButton.y
   sceneGroup:insert(opti)
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
      --Add appropriate button listeners. TODO: Optimize this, perhaps store correct answers in level data.
      if levelNum == 1 then
        buttonA:addEventListener("touch", wrongAnswer)
        buttonB:addEventListener("touch", wrongAnswer)
        buttonC:addEventListener("touch", wrongAnswer)
        buttonD:addEventListener("touch", correctAnswer)
      elseif levelNum == 2 then
        buttonA:addEventListener("touch", wrongAnswer)
        buttonB:addEventListener("touch", wrongAnswer)
        buttonC:addEventListener("touch", correctAnswer)
        buttonD:addEventListener("touch", wrongAnswer)
      elseif levelNum == 3 then
        buttonA:addEventListener("touch", wrongAnswer)
        buttonB:addEventListener("touch", wrongAnswer)
        buttonC:addEventListener("touch", wrongAnswer)
        buttonD:addEventListener("touch", correctAnswer)
      elseif levelNum == 4 then
        buttonA:addEventListener("touch", wrongAnswer)
        buttonB:addEventListener("touch", wrongAnswer)
        buttonC:addEventListener("touch", wrongAnswer)
        buttonD:addEventListener("touch", correctAnswer)
      elseif levelNum == 5 then
        buttonA:addEventListener("touch", wrongAnswer)
        buttonB:addEventListener("touch", correctAnswer)
        buttonC:addEventListener("touch", wrongAnswer)
        buttonD:addEventListener("touch", wrongAnswer)
      elseif levelNum == 6 then
        exit:addEventListener("touch", toTitle)
      else
        --We have encountered an error.
        print("Error when selecting level. (In: SceneShow)")
        print("Level Number: " .. levelNum)
      end
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
      --Remove the appropriate listeners. TODO: optimize like scene show.
      if levelNum == 1 then
        buttonA:removeEventListener("touch", wrongAnswer)
        buttonB:removeEventListener("touch", wrongAnswer)
        buttonC:removeEventListener("touch", wrongAnswer)
        buttonD:removeEventListener("touch", correctAnswer)
      elseif levelNum == 2 then
        buttonA:removeEventListener("touch", wrongAnswer)
        buttonB:removeEventListener("touch", wrongAnswer)
        buttonC:removeEventListener("touch", correctAnswer)
        buttonD:removeEventListener("touch", wrongAnswer)
      elseif levelNum == 3 then
        buttonA:removeEventListener("touch", wrongAnswer)
        buttonB:removeEventListener("touch", wrongAnswer)
        buttonC:removeEventListener("touch", wrongAnswer)
        buttonD:removeEventListener("touch", correctAnswer)
      elseif levelNum == 4 then
        buttonA:removeEventListener("touch", wrongAnswer)
        buttonB:removeEventListener("touch", wrongAnswer)
        buttonC:removeEventListener("touch", wrongAnswer)
        buttonD:removeEventListener("touch", correctAnswer)
      elseif levelNum == 5 then
        buttonA:removeEventListener("touch", wrongAnswer)
        buttonB:removeEventListener("touch", correctAnswer)
        buttonC:removeEventListener("touch", wrongAnswer)
        buttonD:removeEventListener("touch", wrongAnswer)
      elseif levelNum == 6 then
        exit:removeEventListener("touch", toTitle)
      else
        --We have encountered an error.
        print("Error when selecting level. (In: SceneShow)")
        print("Level Number: " .. levelNum)
      end
      composer.removeScene("Code.Game2.game")
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view
   --Clean up.
   background:removeSelf()
   background = nil

   if levelNum ~=6 then
     question:removeSelf()
     question = nil
     buttonA:removeSelf()
     buttonA = nil
     --buttonB:removeSelf()
     --buttonB = nil
     --buttonC:removeself()
     --buttonC = nil
     --buttonD:removeSelf()
     --buttonD = nil
   else
     exit:removeSelf()
     exit = nil
   end

   if displayed then
     --alert:cancelAlert()
     --alert:RemoveSelf()
     alert = nil
     displayed = nil
   end
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
