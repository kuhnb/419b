local composer = require( "composer" )
local scene = composer.newScene()
local myData = require("Code.mydata")
local parse = require("mod_parse")
local widget = require("widget")
local aud = require("Code.Store.audioLocationWriter")
---------------------------------------------------------------------------------

-- local forward references should go here
local background
local playButton
local customPlayButton
local tutorialButton
local creditsButton
local backButton
local title
local learn
local opti
local backi
local optionsButton
--initialize audio locations
getAudioLocation()
local soundEffect = audio.loadSound( "SFX/Common/Menu1.wav" )
local bgmusic = audio.loadStream("SFX/Music/"..myData.audioSelected)
local bgm = audio.loadStream("SFX/Music/"..myData.audioSelected)

---------------------------------------------------------------------------------
--Transition to the actual game.
local function startGame( event )
  --Generate options and parameters
  local options = {
    effect = "fade",
    time = 500,
    params = {
      num = 1,
      level = "2-" .. 1 .. ".png"
    }
  }
  --Go to game.lua
  if event.phase == "ended" then
    audio.stop(1)
    audio.play(bgmusic, {channel=1, loops=-1, fadein = 7500})
    audio.play(soundEffect, {channel=2})
    composer.gotoScene("Code.Game2.game", options)
  end
end

--custom game
local function startCustomGame( event )
  --Generate options and parameters
  local options = {
    effect = "fade",
    time = 500,
    params = {
      num = 1,
      level = "2-" .. 1 .. ".png"
    }
  }
  --Go to game.lua
  if event.phase == "ended" then
    audio.stop(1)
    audio.play(bgmusic, {channel=1, loops=-1, fadein = 7500})
    audio.play(soundEffect, {channel=2})
    composer.gotoScene("Code.Game2.gameV2", options)
  end
end

--Transition to the tutorial.
local function tutStart( event )
  --Generate options.
  local options = {
    effect = "fade",
    time = 500
  }
  --Go to tutorial.lua
  if event.phase == "ended" then
    audio.play(soundEffect, {channel=2})
    composer.gotoScene("Code.Game2.tutorial", options)
  end
end

--Open the credits overlay.
local function openCredits ( event )
  --Generate options
  local options = {
    effect = "fade",
    --This ensures players cannot interact with menu while credits are being displayed.
    isModal = true,
    time = 500
  }
  --Show the credits overlay.
  if event.phase == "ended" then
    audio.play(soundEffect, {channel=2})
    composer.showOverlay("Code.Game2.gamecredits", options)
  end
end

--Hide the credits overlay.
local function hideCredits ()
  if event.phase == "ended" then
    composer.removeScene("Code.Game2.gamecredits", false)
  end
end

--Return to select game screen. So the player can play the good game.
local function toSelect(event)
  --Generate options.
  local options = {
    effect = "fade",
    time = 1000
  }
  --Go to gameSelect.lua
  if event.phase == "ended" then
    audio.play(soundEffect, {channel=2})
    composer.gotoScene("Code.gameSelect", options)
  end
end

--Open options overlay
local function openOptions(event)
  --Generate options
  local options = {
    effect = "fade",
    --This ensures players cannot interact with menu while options are being displayed.
    isModal = true,
    time = 500,
    params = {
      text1 = "Close Options"
    }
  }
  --Show the options overlay.
  if event.phase == "ended" then
    composer.showOverlay("Code.options", options)
  end
end

---------------------------------------------------------------------------------

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view

   --Create and set background image.
   background = display.newImageRect("GFX/Common/title.png", 480, 320)
   background.anchorX = 0
   background.anchorY = 0
   background.x = 0
   background.y = 0
   sceneGroup:insert(background)

   --Create title text. TODO: Fancy Title GFX
   title = display.newText(sceneGroup, "logical.overflow.game2", display.contentCenterX, 40, native.systemFont, 42)
   local options = {
     parent = sceneGroup,
     text = "Solve control flow problems!",
     x = display.contentCenterX,
     y = 100,
     width = 360,
     font = native.systemFont,
     fontSize = 20,
     align = "center"
   }
   learn = display.newText(options)
   learn:setTextColor(0,0,0)

   --Create and set the play button image.
   playButton = widget.newButton{
     left = display.contentCenterX-100,
     top = display.contentCenterY-30,
     label = "Play",
     shape = "roundedRect",
     font = native.systemFont,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = startGame
   }
   sceneGroup:insert(playButton)

   --Create and set the tutorial button image.
   tutorialButton = widget.newButton{
     left = display.contentCenterX-100,
     top = display.contentCenterY+30,
     label = "Tutorial",
     shape = "roundedRect",
     font = native.systemFont,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = tutStart
   }
  sceneGroup:insert(tutorialButton)

   --Create and set the custom play button image.
   customPlayButton = widget.newButton{
     left = display.contentCenterX-100,
     top = display.contentCenterY+90,
     label = "Custom Levels",
     shape = "roundedRect",
     font = native.systemFont,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = startCustomGame
   }
   sceneGroup:insert(customPlayButton)

   --Create and set the back to game select button image.

   backButton = widget.newButton{
     left = display.contentWidth - 43,
     top = display.contentHeight - 43,
     shape = "roundedRect",
     font = native.systemFont,
     width = 40,
     height = 40,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = toSelect
   }
   sceneGroup:insert(backButton)

   backi = display.newImageRect("GFX/Common/left224.png", 35, 35)
   backi.x = backButton.x
   backi.y = backButton.y
   sceneGroup:insert(backi)

   --Create and set an options button
   --Create and set an options button
   optionsButton = widget.newButton{
     left = 0,
     top = display.contentHeight-43,
     shape = "roundedRect",
     font = native.systemFont,
     width = 40,
     height = 40,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = openOptions
   }
   sceneGroup:insert(optionsButton)

   opti = display.newImageRect("GFX/Common/gear74.png", 35, 35)
   opti.x = optionsButton.x
   opti.y = optionsButton.y
   sceneGroup:insert(opti)
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      audio.play(bgm, {channel=1, loops=-1, fadein = 5000})

      --Combo metrics
      --ComboPlay
      if myData.g2Combo then
        if myData.g2RecordCombo then
          if myData.g2RecordCombo < myData.g2Combo then
            myData.g2RecordCombo = myData.g2Combo
            --Send to parse. TODO send to pase
          end
        else
          myData.g2RecordCombo = myData.g2Combo
        end
        myData.g2Combo = 0
      end
      --ComboCorrect
      if myData.g2ComboCorrect then
        if myData.g2RecordComboCorrect then
          if myData.g2RecordComboCorrect < myData.g2ComboCorrect then
            myData.g2RecordComboCorrect = myData.g2ComboCorrect
            --Send to parse. TODO send to pase
          end
        else
          myData.g2RecordComboCorrect = myData.g2ComboCorrect
        end
        myData.g2ComboCorrect = 0
      else
        myData.g2ComboCorrect = 0
        if not myData.g2RecordComboCorrect then
          myData.g2RecordComboCorrect = 0
        end
      end
    if myData.consent == true then
        parse:updateObject("Combo", myData.cid, {["G2Play"] = myData.g2RecordCombo, ["G2Correct"] = myData.g2RecordComboCorrect})
    end
   elseif ( phase == "did" ) then
      --Add appropriate listeners so buttons actually behave like buttons.
      --composer.removeHidden()
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
      --Remove listeners, otherwise code bugs out.
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view
   --Clean up.
   background:removeSelf()
   background = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
