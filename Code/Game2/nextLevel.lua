local composer = require( "composer" )
local scene = composer.newScene()

---------------------------------------------------------------------------------

-- local forward references should go here
  local nextLevel
  local curNum

---------------------------------------------------------------------------------

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   --Save level information.
   curNum = event.params.num
   nextLevel = event.params.level
   print("curNum = " .. curNum)
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
     --Generate options and parameters.
      local options = {
        effect = "fade",
        time = 500,
        params = {
          num = curNum,
          level = nextLevel
        }
      }
      --If its level four the game is currently done and we should go to the title.
      if curNum == 7 then
        audio.stop(1)
        composer.gotoScene("Code.Game2.title", {effect = "fade", time = 1000})
      else
      --Otherwise go to game.lua.
        composer.gotoScene("Code.Game2.game", options)
      end
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
      --Remove this scene to ensure scene:create executes. TODO: optimize this.
      composer.removeScene("Code.Game2.nextLevel")
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view
   --C-c-c-lean up.
   nextLevel = nil
   curNum = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
