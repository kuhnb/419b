local composer = require( "composer" )
local scene = composer.newScene()
local myData = require("Code.mydata")
---------------------------------------------------------------------------------

-- local forward references should go here
local consent
local displayed
local background
local text
local yes
local no

---------------------------------------------------------------------------------

--generic function to write to file
local function writeToFile(con)
  local path = system.pathForFile("consent.txt", system.DocumentsDirectory)
  local file, errorString = io.open( path,  "w")
  local text = con
  if not file then
    --print("File error: " .. errorString)
    file:write(text)
    io.close(file)
  else
    file:write(text)
    io.close(file)
  end
  file = nil

end

--Generic function to check if a particular file exists
local function doesFileExist( fname, path )
    local results = false
    -- Path for the file
    local filePath = system.pathForFile( fname, path )
    if ( filePath ) then
        local file, errorString = io.open( filePath, "r" )

        if not file then
            -- Error occurred; output the cause
            print( "File error: " .. errorString )
        else
            -- File exists!
            print( "File found: " .. fname )
            local contents = file:read("*l")
            if fname == "consent.txt" then
              myData.consent = contents
              print("Consent Status: " .. tostring(myData.consent))
            end
            if fname == "options.txt" then

            end
            results = true
            -- Close the file handle
            file:close()
        end
    end
    return results
end

local function loadOptions(event)
  local filePath = system.pathForFile("options.txt", system.DocumentsDirectory)
  if ( filePath ) then
      local file, errorString = io.open( filePath, "r" )

      if not file then
          -- Error occurred; output the cause
          print( "File error: " .. errorString )
      else
          local contents = file:read("*l")
          myData.music = tonumber(string.sub(contents, 7))
          contents = file:read("*l")
          myData.sfx = tonumber(string.sub(contents, 5))
          print("Music Volume: " .. myData.music)
          print("SFX Valume: " .. myData.sfx)
          -- Close the file handle
          file:close()
          audio.setVolume(myData.music, {channel=1})
          audio.setVolume(myData.sfx, {channel=2})
      end
  end
end

local function createOptions(event)
  local path = system.pathForFile("options.txt", system.DocumentsDirectory)
  local file, errorString = io.open( path,  "w")
  if not file then
    print("File error: " .. errorString)
    io.close(file)
  else
    file:write("music=0.5\nsfx=0.5")
    io.close(file)
    loadOptions()
  end
  file = nil
end

function setConsent(event)
  if event.phase == "ended" and not doesFileExist("consent.txt", system.DocumentsDirectory) then

  else
    writeToFile("true")
    myData.consent = true
    print("Consent Status: " .. tostring(myData.consent))
    local options = {
      effect = "fade",
      time = 500,
      params = {
        conCreate = true
      }
    }
    composer.gotoScene("Code.setupParse", options)
  end
end

function denyConsent(event)
  if event.phase == "ended" and not doesFileExist("consent.txt", system.DocumentsDirectory) then
    writeToFile("false")
    myData.consent = false
    print("Consent Status: " .. tostring(myData.consent))
    local options = {
      effect = "fade",
      time = 500,
      params = {
        conCreate = true
      }
    }
    composer.gotoScene("Code.setupParse", options)
  end
end

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view

   background = display.newImageRect("GFX/Common/title.png", 480, 320)
   background.anchorX = 0
   background.anchorY = 0
   background.x = 0
   background.y = 0
   sceneGroup:insert(background)

   title = display.newText(sceneGroup, "Can we collect information on how you play our game?", display.contentCenterX, display.contentCenterY-50, native.systemFont, 18)
   yes = display.newText(sceneGroup, "Yes", display.contentCenterX-50, display.contentCenterY+50, native.systemFont, 24)
   no = display.newText(sceneGroup, "No", display.contentCenterX+50, display.contentCenterY+50, native.systemFont, 24)

end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
     yes:addEventListener("touch", setConsent)
     no:addEventListener("touch", denyConsent)
     if doesFileExist("options.txt", system.DocumentsDirectory) then
       loadOptions()
     else
       createOptions()
     end
     if doesFileExist("consent.txt", system.DocumentsDirectory) then
       composer.gotoScene("Code.setupParse")
     end
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
     yes:removeEventListener("touch", setConsent)
     no:removeEventListener("touch", denyConsent)
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


---------------------------------------------------------------------------------

return scene
