local composer = require( "composer" )
local scene = composer.newScene()

---------------------------------------------------------------------------------

-- local forward references should go here
local credits
local shame = "You are Dr. Claz. You have been tasked to find the scientists that were abducted by the evil Dr. Overflow. To do so you must triangulate the scientists cell phones. Unfortunately it appears as though Dr. Overflow has taken control of all of the world's communication towers. So now you must hack into all of the communication towers by solving boolean and control flow problems. \nEnd of amazing plot."
local shame2

---------------------------------------------------------------------------------

--Close the credits. Who wants their name on the project?
function closeCredits( event )
  --Hide the overlay with the appropriate options
  if event.phase == "ended" then
    composer.hideOverlay("fade", 100)
  end
end

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view

   -- Initialize the scene here.
   -- Example: add display objects to "sceneGroup", add touch listeners, etc.

   --Yes I jsut copy pasted credits.lua. Deal with it.
   credits = display.newImageRect("GFX/Common/whitebg.png", 440, 320)
   credits.x = display.contentCenterX
   credits.y = display.contentCenterY
   credits:setFillColor(0.5, 0.60, 0.5, 1)
   credits.strokeWidth = 4
   credits:setStrokeColor( 0, 0 ,0 )
   sceneGroup:insert(credits)

   --Text
   local options = {
     parent = sceneGroup,
     text = shame,
     x = display.contentCenterX,
     y = display.contentCenterY,
     font = native.systemFont,
     fontSize = 16,
     width = 360,
     align = "center"
   }
   shame2 = display.newText(options)
   shame2:setTextColor(0,0,0)

   options = {
     parent = sceneGroup,
     text = "The Story So Far\n---------------------------------------------------------------",
     x = display.contentCenterX,
     y = 25,
     font = native.systemFontBold,
     fontSize = 20,
     align = "center"
   }
   h1 = display.newText(options)
   h1:setTextColor(0,0,0)
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase
   local parent = event.parent

   if ( phase == "will" ) then
      -- Called when the scene is still off screen (but is about to come on screen).
   elseif ( phase == "did" ) then
      -- Called when the scene is now on screen.
      -- Insert code here to make the scene come alive.
      -- Example: start timers, begin animation, play audio, etc.

      --Make it so the user can click the image to dismiss the credits.
      credits:addEventListener("touch", closeCredits)
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase
   local parent = event.parent

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
      --Remove the listener.
      credits:removeEventListener("touch", closeCredits)
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

   -- Called prior to the removal of scene's view ("sceneGroup").
   -- Insert code here to clean up the scene.
   -- Example: remove display objects, save state, etc.

   --Clean up the variable(s).
   credits:removeSelf()
   credits = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
