local composer = require( "composer" )
local scene = composer.newScene()
local myData = require("Code.mydata")
local userCheck = require("app42UserCheck")
require("Code.boughtResources")

local scoreSaver = require("Code.Leaderboard.scoreSaver")
require("Code.currency")
require("Code.Store.graphicsLocationWriter")
require("Code.Store.audioLocationWriter")
require("Code.UserContent.downloadCustomGameLevel")
local g2reader=require("Code.Game2.gameReader")
require("Code.aliasLocationWriter")
---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here
local game1
local game2
local leaderboard
local background
local title
local goal
local soundEffect = audio.loadSound( "SFX/Common/Menu1.wav" )
local bgm
local optionsButton
local opti
local multiplayerButton
local plot
local credits
local widget = require("widget")


---------------------------------------------------------------------------------
--Go to Game1 (Logic Circuits)
local function toGame1(event)
  local options = {
    effect = "fade",
    time = 1000
  }
  if event.phase == "ended" then
    audio.play(soundEffect, {channel=2})
    composer.gotoScene("Code.Game1.title", options)
  end
end

--Go to Game2 (Filling out Reqs)
local function toGame2(event)
  local options = {
    effect = "fade",
    time = 1000
  }
  if event.phase == "ended" then
    audio.play(soundEffect, {channel=2})
    composer.gotoScene("Code.Game2.title", options)
  end
end

--Go to Leaderboard (IN DEVELOPMENT... ;-)  )
local function toLeaderboard(event)
  local options = {
    effect = "fade",
    time = 1000
  }
  if event.phase == "ended" then
    audio.play(soundEffect, {channel=2})
    --composer.gotoScene("Code.Leaderboard.dummyScene", options) -- //!@# change this location


    composer.gotoScene("Code.Leaderboard.leaderboard", options)
  end
end

--Go to the hoop jump that is multiplayer
local function toMultiplayer(event)
  local options = {
    effect = "fade",
    time = 1000
  }
  if event.phase == "ended" then
    audio.play(soundEffect, {channel=2})
    composer.gotoScene("Code.Multiplayer.title", options)
  end
end

--Go to the store
local function toStore(event)
  local options = {
    effect = "fade",
    time = 1000
  }
  if event.phase == "ended" then
    audio.play(soundEffect, {channel=2})
    composer.gotoScene("Code.Store.storeBase", options)
  end
end


--Open options overlay
local function openOptions(event)

  --Generate options
  local options = {
    effect = "fade",
    --This ensures players cannot interact with menu while options are being displayed.
    isModal = true,
    time = 500,
    params = {
      text1 = "Close Options"
    }
  }
  --Show the options overlay.
  if event.phase == "ended" then
    composer.showOverlay("Code.options", options)
  end
end

local function initializeStore()
  getResourcesArray()
  myData.graphicsSelected = getGraphicsLocation()
  myData.audioSelected = getAudioLocation()
  print()
  bgm = audio.loadStream("SFX/Music/"..myData.audioSelected)
end

--Open the credits overlay.
local function openCredits ( event )
  --Generate options
  local options = {
    effect = "fade",
    --This ensures players cannot interact with menu while credits are being displayed.
    isModal = true,
    time = 500
  }
  --Show the credits overlay.
  if event.phase == "ended" then
    audio.play(soundEffect, {channel=2})
    composer.showOverlay("Code.gamecredits", options)
  end
end

--Open the credits overlay.
local function openPlot ( event )
  --Generate options
  local options = {
    effect = "fade",
    --This ensures players cannot interact with menu while credits are being displayed.
    isModal = true,
    time = 500
  }
  --Show the credits overlay.
  if event.phase == "ended" then
    audio.play(soundEffect, {channel=2})
    composer.showOverlay("Code.plot", options)
  end
end

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
    --Download File (Initialize Game Reader called after, to prevent read of unapproved changed local file)
    cgl_downloadFile()
    
    --ScoreSaver
    runInitialTest()

    --GameReader initialized in networkListener of cgl file( on phase "ended")

   --Currency: Before creating scene, should load in user's currency
    initializeCurrency()

    --Store
    initializeStore()

    --alias before userAuth
    local al=getAlias()
    print("Alias:"..tostring(al))
   --App42: Before creating the Scene, should authenticate the user with App42
    if (myData.consent == "true") then
      userAuthenticate()
    end


   -- Initialize the scene here.
   -- Example: add display objects to "sceneGroup", add touch listeners, etc.
   background = display.newImageRect("GFX/Common/title.png", 480, 320)
   background.anchorX = 0
   background.anchorY = 0
   background.x = 0
   background.y = 0
   sceneGroup:insert(background)

   --Generate and set text
   title = display.newText(sceneGroup, "logical.overflow", display.contentCenterX, 40, native.systemFont, 42)
   options = {
     parent = sceneGroup,
     text =  "Welcome to logical.overflow! \nReady to solve boolean related questions?",
     x = display.contentCenterX,
     y = 100,
     width = 440,
     font = native.systemFont,
     fontSize = 20,
     align = "center"
   }
   goal = display.newText(options)
   goal:setTextColor(0,0,0)


   game1 = widget.newButton{
     left = (display.contentWidth-200)/2-100,
     top = display.contentCenterY-25,
     label = "Play Game 1",
     shape = "roundedRect",
     font = native.systemFont,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = toGame1
   }
   sceneGroup:insert(game1)


   game2 = widget.newButton{
     left = (display.contentWidth-200)/2-100,
     top = display.contentCenterY+35,
     label = "Play Game 2",
     shape = "roundedRect",
     font = native.systemFont,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = toGame2
   }
   sceneGroup:insert(game2)

   --Leaderboard button
   leaderboard = widget.newButton{
     left = (display.contentWidth-200)/2+100,
     top = display.contentCenterY+35,
     label = "Leaderboards",
     shape = "roundedRect",
     font = native.systemFont,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = toLeaderboard
   }
   sceneGroup:insert(leaderboard)

   credits = widget.newButton{
     left = (display.contentWidth-200)/2-5,
     top = display.contentCenterY+95,
     label = "Credits",
     shape = "roundedRect",
     font = native.systemFont,
     width = 85,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = openCredits
   }
   sceneGroup:insert(credits)

   plot = widget.newButton{
     left = (display.contentWidth-200)/2+100,
     top = display.contentCenterY+95,
     label = "Plot",
     shape = "roundedRect",
     font = native.systemFont,
     width = 85,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = openPlot
   }
   sceneGroup:insert(plot)
   --Create and set an options button
   optionsButton = widget.newButton{
     left = 0,
     top = display.contentHeight-43,
     shape = "roundedRect",
     font = native.systemFont,
     width = 40,
     height = 40,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = openOptions
   }
   sceneGroup:insert(optionsButton)

   opti = display.newImageRect("GFX/Common/gear74.png", 35, 35)
   opti.x = optionsButton.x
   opti.y = optionsButton.y
   sceneGroup:insert(opti)

   --Create bullshit multiplayer button. Not all games need multipler... its a toxic trend...
   multiplayerButton = widget.newButton{
     left = (display.contentWidth-200)/2+100,
     top = display.contentCenterY-25,
     label = "Multiplayer",
     shape = "roundedRect",
     font = native.systemFont,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = toMultiplayer
   }
   sceneGroup:insert(multiplayerButton)

      --Store button
   store = widget.newButton{
     left = (display.contentWidth-200)/2+196,
     top = display.contentCenterY+95,
     label = "Store",
     shape = "roundedRect",
     font = native.systemFont,
     width = 85,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = toStore
   }
   sceneGroup:insert(store)
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is still off screen (but is about to come on screen).
   elseif ( phase == "did" ) then
      --composer.removeScene("Code.Multiplayer.title")
      audio.play(bgm, {channel=1, loops=-1})
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is on screen (but is about to go off screen).
      -- Insert code here to "pause" the scene.
      -- Example: stop timers, stop animation, stop audio, etc.
   elseif ( phase == "did" ) then
      -- Called immediately after scene goes off screen.
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view
   --Do as Barney does and clean up.
   background:removeSelf()
   background = nil
   audio.stop()
   audio.dispose(soundEffect)
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


---------------------------------------------------------------------------------

return scene
