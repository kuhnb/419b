local composer = require( "composer" )
local scene = composer.newScene()
local widget = require( "widget" )
local myData = require("Code.mydata")
require("Code.Game2.gameReader")
require("Code.UserContent.downloadCustomGameLevel")

---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here

---------------------------------------------------------------------------------

local background
local title
local backButton
local Question
local QuestionText
local answerA
local answerAText
local answerB
local answerBText
local answerC
local answerCText
local answerD
local answerDText
local answerGroup= {}
local answerGroupSelected
local difficultyGroup = {}
local difficultyGroupSelected
local letters = {}
letters[1] = "A"
letters[2] = "B"
letters[3] = "C"
letters[4] = "D"
local difficulties = {}
difficulties[1] = "Easy"
difficulties[2] = "Medium"
difficulties[3] = "Hard"
local submitButton
local errorText = {}
-----------------
local changeAnswer
local changeDifficulty
--------------------------------
--Return to select game screen. So the player can play the good game.
local function toSelect()
  --Generate options.
  local options = {
  effect = "fade",
  time = 1000
  }
  audio.play(soundEffect, {channel=2})
  composer.gotoScene("Code.Store.storeBase", options)
end

local function textListener( event )

    if ( event.phase == "began" ) then
        -- user begins editing defaultField
        print( event.text )

    elseif ( event.phase == "ended" or event.phase == "submitted" ) then
        -- do something with defaultField text
        print( event.target.text )

    elseif ( event.phase == "editing" ) then
        print( event.newCharacters )
        print( event.oldText )
        print( event.startPosition )
        print( event.text )
    end
end

local function createTextFields()
  sceneGroup = scene.view
  local textSize = 20
  local textboxHeight = 20
  local distance = textboxHeight
  local start = 30
  QuestionText = display.newText(sceneGroup, "Question:", display.contentCenterX - 100, start + distance, native.systemFont, textSize)
  Question = native.newTextField( display.contentCenterX + 100, start + distance, 180, textboxHeight )

  answerAText = display.newText(sceneGroup, "answerA:", display.contentCenterX - 100, start + distance*2, native.systemFont, textSize)
  answerA = native.newTextField( display.contentCenterX + 100, start + distance*2, 180, textboxHeight )

  answerBText = display.newText(sceneGroup, "answerB:", display.contentCenterX - 100, start + distance*3, native.systemFont, textSize)
  answerB = native.newTextField( display.contentCenterX + 100, start + distance*3, 180, textboxHeight )

  answerCText = display.newText(sceneGroup, "answerC:", display.contentCenterX - 100, start + distance*4, native.systemFont, textSize)
  answerC = native.newTextField( display.contentCenterX + 100, start + distance*4, 180, textboxHeight )

  answerDText = display.newText(sceneGroup, "answerD:", display.contentCenterX - 100, start + distance*5, native.systemFont, textSize)
  answerD = native.newTextField( display.contentCenterX + 100, start + distance*5, 180, textboxHeight )


  sceneGroup:insert(Question)
  sceneGroup:insert(answerA)
  sceneGroup:insert(answerB)
  sceneGroup:insert(answerC)
  sceneGroup:insert(answerD)
end




local function createanswerButton(index)
  local fill = { default={ 0.2, 0.27, 0.24}, over={ 0.2, 0.27, 0.24} }
  local name = letters[index]
  event = changeAnswer(index)
  if(answerGroupSelected == letters[index]) then
    fill = { default={ 0.1, 0.1, 0.1}, over={ 0.1, 0.1, 0.1} }
    event = nil
  end
  return widget.newButton{
          label = name,
          left = (index*(display.contentWidth/5)-15),
          id = name..index,
          top = 0,
         onRelease = event,
          --properties for a rounded rectangle button...
         shape="roundedRect",
         width = 30,
         height = 30,
         cornerRadius = 2,
         fillColor = fill,
         strokeColor = { default={ 0,0, 0 }, over={ 0,0,0} },
         strokeWidth = 4
      }
end


local function createDifficultyButton(index)
  local fill = { default={ 0.2, 0.27, 0.24}, over={ 0.2, 0.27, 0.24} }
  local name = difficulties[index]
  event = changeDifficulty(index)
  if(difficultyGroupSelected==difficulties[index]) then
    fill = { default={ 0.1, 0.1, 0.1}, over={ 0.1, 0.1, 0.1} }
    event = nil
  end
  return widget.newButton{
          label = name,
          left = (index*(display.contentWidth/4)-45 ),
          id = name..index,
          top = 0,
         onRelease = event,
          --properties for a rounded rectangle button...
         shape="roundedRect",
         width = 90,
         height = 30,
         cornerRadius = 2,
         fillColor = fill,
         strokeColor = { default={ 0,0, 0 }, over={ 0,0,0} },
         strokeWidth = 4
      }
end

local function createDifficultyButtons()
  local sceneGroup = scene.view
    difficultyGroup[1] = display.newGroup()
    for i=1,3,1 do
      if(difficultyGroup[i + 1] ~= nil) then
        difficultyGroup[i + 1]:removeSelf()
        difficultyGroup[i + 1] = nil
      end
      difficultyGroup[i + 1] = createDifficultyButton(i)
      difficultyGroup[1]:insert(difficultyGroup[i + 1])
    end
    difficultyGroup[1].y = display.contentHeight - 100
    sceneGroup:insert(difficultyGroup[1])
end

local function createanswerButtons()
  local sceneGroup = scene.view
  answerGroup[1] = display.newGroup()
  for i=1,4,1 do
    if(answerGroup[i + 1] ~= nil) then
      answerGroup[i + 1]:removeSelf()
      answerGroup[i + 1] = nil
    end
    answerGroup[i + 1] = createanswerButton(i)
    answerGroup[1]:insert(answerGroup[i + 1])
  end
  answerGroup[1].y = display.contentHeight - 140
  sceneGroup:insert(answerGroup[1])
end

changeAnswer=function(index)
  return function()
    print(index.." "..letters[index])
    answerGroupSelected = letters[index]
    createanswerButtons()
  end
end
changeDifficulty=function(index)
  return function()
    difficultyGroupSelected = difficulties[index]
    createDifficultyButtons()
  end
end

local attemptSubmit=function()
print("attemptSubmit method")
  if Question==nil or Question=="" then
    errorText.text = "Please create a question"
    return nil
  end

  if answerA == nil or answerA == "" then
    errorText.text = "Please create answer A"
    return nil
  end

  if answerB == nil or answerB == "" then
    errorText.text = "Please create answer B"
    return nil
  end

  if answerC == nil or answerC == "" then
    errorText.text = "Please create answer C"
    return nil
  end

  if answerD == nil or answerD == "" then
    errorText.text = "Please create answer D"
    return nil
  end

  if answerGroupSelected == nil then
    errorText.text = "Please Select an answer"
    return nil
  end

  if difficultyGroupSelected == nil then
    errorText.text = "Please Select a dificulty"
    return nil
  end

  local text = "Custom\n"..tostring(Question.text).."\n"..tostring(answerA.text).."\n"..tostring(answerB.text).."\n"..tostring(answerC.text).."\n"..tostring(answerD.text).."\n"..answerGroupSelected.."\n"..difficultyGroupSelected.."\n"
  print("TEST TEXT:\n"..text)
  gR_addToFile(text)
  --read not necessary after write
  errorText.text = "Question Succesfully Created"
  cgl_uploadFile()
end

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view

   --background
   background = display.newImageRect("GFX/Common/title.png", 480, 320)
   background.anchorX = 0
   background.anchorY = 0
   background.x = 0
   background.y = 0
   sceneGroup:insert(background)

    --Title
    title = display.newText(sceneGroup, "Make your own question", display.contentCenterX, 15, native.systemFont, 24)
    sceneGroup:insert(title)

    --Answer/Difficulty buttons
    createanswerButtons()
    createDifficultyButtons()
    --back button
    backButton = widget.newButton{
              label = "To Menu",
              left = display.contentWidth - 90,
              id = "Main",
              top = display.contentHeight - 42,
             onRelease = toSelect,
              --properties for a rounded rectangle button...
             shape="roundedRect",
             width = 90,
             height = 40,
             cornerRadius = 2,
             fillColor = { default={ 0.2, 0.27, 0.24}, over={ 0.2, 0.27, 0.24} },
             strokeColor = { default={ 0,0, 0 }, over={ 0,0,0} },
             strokeWidth = 4
          }
              sceneGroup:insert(backButton)
      --submit button
      submitButton = widget.newButton{
              label = "Submit",
              left = display.contentCenterX - 45,
              id = "submit",
              top = display.contentHeight - 42,
             onRelease = attemptSubmit,
              --properties for a rounded rectangle button...
             shape="roundedRect",
             width = 90,
             height = 40,
             cornerRadius = 2,
             fillColor = { default={ 0.2, 0.27, 0.24}, over={ 0.2, 0.27, 0.24} },
             strokeColor = { default={ 0,0, 0 }, over={ 0,0,0} },
             strokeWidth = 4
          }
          errorText = display.newText(sceneGroup, "", display.contentCenterX, display.contentHeight - 55, native.systemFont, 24)
              sceneGroup:insert(submitButton)
   -- Create text field1
   -- Initialize the scene here.
   -- Example: add display objects to "sceneGroup", add touch listeners, etc.
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
     errorText.txt = ""
      -- Called when the scene is still off screen (but is about to come on screen).
   elseif ( phase == "did" ) then
   createTextFields()


   sceneGroup:insert(Question)
   sceneGroup:insert(answerA)
   sceneGroup:insert(answerB)
   sceneGroup:insert(answerC)
   sceneGroup:insert(answerD)
      -- Called when the scene is now on screen.
      -- Insert code here to make the scene come alive.
      -- Example: start timers, begin animation, play audio, etc.
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
   Question:removeSelf()
   answerA:removeSelf()
   answerB:removeSelf()
   answerC:removeSelf()
   answerD:removeSelf()

   Question = nil
   answerA = nil
   answerB = nil
   answerC = nil
   answerD = nil
      -- Called when the scene is on screen (but is about to go off screen).
      -- Insert code here to "pause" the scene.
      -- Example: stop timers, stop animation, stop audio, etc.
   elseif ( phase == "did" ) then
      -- Called immediately after scene goes off screen.
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

   -- Called prior to the removal of scene's view ("sceneGroup").
   -- Insert code here to clean up the scene.
   -- Example: remove display objects, save state, etc.
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
