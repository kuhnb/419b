local composer = require( "composer" )
local scene = composer.newScene()
local widget = require( "widget" )
local myData = require("Code.mydata")
require("Code.currency")
require("Code.Leaderboard.scoreSaver")
---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here

local background
local title
local money
local backButton
local graphics
local sounds
local levelsButton
--
local buttonWidth = 200
local buttonHeight = 40
---------------------------------------------------------------------------------

   --Return to select game screen. So the player can play the good game.
local function toSelect()
 --Generate options.
 local options = {
   effect = "fade",
   time = 1000
 }
   audio.play(soundEffect, {channel=2})
   composer.gotoScene("Code.gameSelect", options)
end

local function openScene(name)

    --Generate options
    local options = {
      effect = "fade",
      --This ensures players cannot interact with menu while options are being displayed.
      isModal = true,
      time = 250
    }
    return function()
        composer.gotoScene(name, options)
    end
end

local function createButtons()


    backButton = widget.newButton{
            label = "To Menu",
            left = display.contentWidth - 95,
            id = "Main",
            top = display.contentHeight - 45,
           onRelease = toSelect,
            --properties for a rounded rectangle button...
           shape="roundedRect",
           width = 90,
           height = 40,
           cornerRadius = 2,
           labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
           fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
           strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
           strokeWidth = 4
        }
    graphics = widget.newButton{
            label = "Graphics",
            x = display.contentCenterX,
            id = "graphics",
            y = display.contentCenterY -70,
           onRelease = openScene("Code.Store.graphics"),
            --properties for a rounded rectangle button...
           shape="roundedRect",
           width = buttonWidth,
           height = buttonHeight,
           cornerRadius = 2,
           labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
           fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
           strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
           strokeWidth = 4
        }
    sounds = widget.newButton{
            label = "Music",
            x = display.contentCenterX,
            id = "sound",
            y = display.contentCenterY ,
           onRelease = openScene("Code.Store.audio"),
            --properties for a rounded rectangle button...
           shape="roundedRect",
           width = buttonWidth,
           height = buttonHeight,
           cornerRadius = 2,
           labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
           fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
           strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
           strokeWidth = 4
        }
end

local function createCustomButton(result)
  sceneGroup = scene.view
  print("Create Custom Button\nRank:"..result[1][1].."\nuser:"..myData.App42Username.."\n"..tostring(result[1][1] == myData.App42Username))
  if(result[1][1] == myData.App42Username) then
    levelsButton= widget.newButton{
        label = "Custom Levels",
        x = display.contentCenterX,
        id = "custom",
        y = display.contentCenterY +70,
       onRelease = openScene("Code.UserContent.InputPage"), --I vs. i
        --properties for a rounded rectangle button...
       shape="roundedRect",
       width = buttonWidth,
       height = buttonHeight,
       cornerRadius = 2,
       labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
       fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
       strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
       strokeWidth = 4
    }
  else
      levelsButton= widget.newButton{
          label = "Become a champion first",
          x = display.contentCenterX,
          id = "custom",
          y = display.contentCenterY +70,
          --properties for a rounded rectangle button...
         shape="roundedRect",
         width = buttonWidth,
         height = buttonHeight,
         cornerRadius = 2,
         labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
         fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
         strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
         strokeWidth = 4
      }
  end
  print("End")
  sceneGroup:insert(levelsButton)
end
-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view

   background = display.newImageRect("GFX/Common/title.png", 480, 320)
   background.anchorX = 0
   background.anchorY = 0
   background.x = 0
   background.y = 0
   sceneGroup:insert(background)

   title = display.newText(sceneGroup, "Store", 50, 30, native.systemFont, 42)
   createButtons()
   sceneGroup:insert(backButton)
   sceneGroup:insert(graphics)
   sceneGroup:insert(sounds)
   getTopNRankers("Global",1, createCustomButton)

   -- Initialize the scene here.
   -- Example: add display objects to "sceneGroup", add touch listeners, etc.
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
    --update currency
   initializeCurrency()
   local options = {
     effect = "fade",
     isModal = true,
     time = 500,
   }
   money = display.newText(sceneGroup, "Coins "..tostring(myData.coins), display.contentWidth - 80, 30,  native.systemFont, 24)
      -- Called when the scene is still off screen (but is about to come on screen).
   elseif ( phase == "did" ) then
      -- Called when the scene is now on screen.
      -- Insert code here to make the scene come alive.
      -- Example: start timers, begin animation, play audio, etc.
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is on screen (but is about to go off screen).
      -- Insert code here to "pause" the scene.
      -- Example: stop timers, stop animation, stop audio, etc.
   elseif ( phase == "did" ) then
     money:removeSelf()
      -- Called immediately after scene goes off screen.
      money = nil
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

   -- Called prior to the removal of scene's view ("sceneGroup").
   -- Insert code here to clean up the scene.
   -- Example: remove display objects, save state, etc.
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
