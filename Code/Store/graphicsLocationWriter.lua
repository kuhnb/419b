--graphics.lua
--this file will be responsible for providing the string of the current location of the graphics to be loaded
--ex. On game start, get the graphics from the location specified in the grphics.txt file
--------------------------------------------------
local composer = require( "composer" )
local myData = require("Code.mydata")
--------------------------------------------------
--NAME OF THE GRAPHICS FILE
local nameOfFile="graphics.txt"
--Default GFX path
local defaultGFXpath="GFX/Game1/"
local customGFXpath="GFX/"



--this function is to be called at the begining of Logical Overflow. It will fetch the location of the graphics (GFX location) to use
function getGraphicsLocation()
  local graphicsPath

  --First, check if file exists
	local graphicsLocation =  graphicsLocationWriter_doesFileExist(nameOfFile, system.DocumentsDirectory)
  --print("graphicsLocationBoolean:"..tostring(graphicsLocation))
	if graphicsLocation then
		--A specific GFX path exists already (possibly for non-default resource). Use that path
    --Read the path from the file
    graphicsPath = graphicsLocationWriter_readFile(nameOfFile,system.DocumentsDirectory)
	else
    --A GFX path does not exist (given by lack of graphics.txt presence). Re-instantiate default package, using defaultGFXpath
    --write the file with the default path
    print("Creating Graphics File.")
    graphicsLocationWriter_writeFile(defaultGFXpath)
    --then use the default path
    graphicsPath = defaultGFXpath
  end

  --return the graphicsPath
  return graphicsPath
end

--function to check if graphics file exists
function graphicsLocationWriter_doesFileExist( fname, path )
    local result = false
    -- Path for the file
    local filePath = system.pathForFile( fname, path )
    if ( filePath ) then
        local file, errorString = io.open( filePath, "r" ) --read
        if file then
          result=true
        end
    end
    --return boolean
    return result
end

--function to read graphics file
function graphicsLocationWriter_readFile(fname, path)
  local result = ""
  local filePath = system.pathForFile( fname, path )
    if ( filePath ) then
        local file, errorString = io.open( filePath, "r" ) --read

        if not file then
            -- Error occurred; output the cause
            print( "Graphics File Read Error.")
        else
            -- File exists! Read from file
            if fname == nameOfFile then
              --read one line
              result = file:read("*l") --Reads the line from the current file position, and moves file position to next line.
              --Reset myData.graphicsSelected
              myData.graphicsSelected=result
            end
            --print( "Graphics File Found: " .. fname.."\n\tValue: "..myData.GFX.location)
        end
        -- Close the file handle
        file:close()
    end
  --return result
  return result

end

--function to write new graphics path to graphics file.
function graphicsLocationWriter_writeFile(text)
  local path = system.pathForFile(nameOfFile, system.DocumentsDirectory)
  local file, errorString = io.open( path,  "w+") --All existing data is removed if file exists or new file is created with read write permissions.
  if not file then
    --Error not handled
    --print("File error: " .. errorString)
    file:write(text)
    io.close(file)
  else
    file:write(text)
    io.close(file)
  end
  file = nil

  --read text after write, to update myData
  graphicsLocationWriter_readFile(nameOfFile,system.DocumentsDirectory)

end

function graphicsLocationWriter_write()
  graphicsLocationWriter_writeFile(myData.graphicsSelected)
end
