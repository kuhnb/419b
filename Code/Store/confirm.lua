local composer = require( "composer" )
local scene = composer.newScene()
local widget = require( "widget" )

---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here

local background
local title
local confirm
local index
local yesButton
local noButton
---------------------------------------------------------------------------------


local function closeOverlay(c)
  local con
  if(c) then
    con = true
  else
    con = false
  end
  return function()
    confirm = con
    composer.hideOverlay("fade", 250)
  end
end

local function createButtons()

    yesButton = widget.newButton{
        label = "Yes",
        x = display.contentCenterX - 40,
        id = "yes",
        y = display.contentCenterY + 10,
       onRelease = closeOverlay(true),
        --properties for a rounded rectangle button...
       shape="roundedRect",
       width = 50,
       height = 40,
       cornerRadius = 2,
       fillColor = { default={ 0.2, 0.27, 0.24}, over={ 0.2, 0.27, 0.24} },
       strokeColor = { default={ 0,0, 0 }, over={ 0,0,0} },
       strokeWidth = 4
    }
    noButton = widget.newButton{
        label = "No",
        x = display.contentCenterX + 40,
        id = "no",
        y = display.contentCenterY + 10,
       onRelease = closeOverlay(false),
        --properties for a rounded rectangle button...
       shape="roundedRect",
       width = 50,
       height = 40,
       cornerRadius = 2,
       fillColor = { default={ 0.2, 0.27, 0.24}, over={ 0.2, 0.27, 0.24} },
       strokeColor = { default={ 0,0, 0 }, over={ 0,0,0} },
       strokeWidth = 4
    }
end
-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   index = event.params.index
   background = display.newRect( display.contentCenterX, display.contentCenterY - 15, 200, 140 )
   background.strokeWidth = 3
   background:setFillColor(  0.2, 0.27, 0.24 )
   background:setStrokeColor( 0, 0, 0 )
   sceneGroup:insert(background)

   title = display.newText(sceneGroup, "Confirm Buy", display.contentCenterX, display.contentCenterY - 50, native.systemFont, 18)
   createButtons()
   sceneGroup:insert(yesButton)
   sceneGroup:insert(noButton)

   -- Initialize the scene here.
   -- Example: add display objects to "sceneGroup", add touch listeners, etc.
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
   local options = {
     effect = "fade",
     isModal = true,
     time = 500,
   }
      -- Called when the scene is still off screen (but is about to come on screen).
   elseif ( phase == "did" ) then
      -- Called when the scene is now on screen.
      -- Insert code here to make the scene come alive.
      -- Example: start timers, begin animation, play audio, etc.
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   local parent = event.parent
   if ( phase == "will" ) then
      -- Called when the scene is on screen (but is about to go off screen).
      -- Insert code here to "pause" the scene.
      -- Example: stop timers, stop animation, stop audio, etc.
   elseif ( phase == "did" ) then
      -- Called immediately after scene goes off screen.
      parent:confirmed(index, confirm)
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

   -- Called prior to the removal of scene's view ("sceneGroup").
   -- Insert code here to clean up the scene.
   -- Example: remove display objects, save state, etc.
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
