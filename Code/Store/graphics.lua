local composer = require( "composer" )
local scene = composer.newScene()
local widget = require( "widget" )
local myData = require("Code.mydata")
require("Code.Store.graphicsLocationWriter")
require("Code.currency")
require("Code.boughtResources")

---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here

local background
local exitButton
local items = {}
local title
local money
local moneyForCoolLeg = {}
moneyForCoolLeg[1] = 0
moneyForCoolLeg[2] = 250
moneyForCoolLeg[3] = 750
---------------------------------------------------------------------------------
local function confirm(ind)
  --Generate options
  local options = {
    effect = "fade",
    --This ensures players cannot interact with menu while options are being displayed.
    isModal = true,
    time = 250,
    params = {
      index = ind
    }
  }
  return function()
    composer.showOverlay("Code.Store.confirm", options)
  end
end

local function setUpLocations()
  myData.graphicsLocation = {}
  myData.graphicsLocation[1] = "GFX/Game1/"
  myData.graphicsLocation[2] = "GFX/Game1Set1/"
  myData.graphicsLocation[3] = "GFX/Game1Set2/"
--  myData.graphicsLocation[2] = "GFX/Game1Set1/"
--  myData.graphicsLocation[3] = "GFX/Game1Set2/"
end

function scene:confirmed(index, confirm)
  print("Confirmed "..tostring(confirm))
  if(confirm) then
    local sceneGroup = self.view
    isTranactionOkay(moneyForCoolLeg[index])
    money:removeSelf()
    money = nil
    money = display.newText(sceneGroup, "Coins "..tostring(myData.coins), display.contentWidth - 80, 30, native.systemFont, 24)
    myData.graphicsBought[index] = true
    writeBought()
    print("New Graphic Bought! "..index)
    self:changeSelect(index)()
  end
end


function scene:changeSelect(index)
  return function()
    myData.graphicsSelected = myData.graphicsLocation[index]
    graphicsLocationWriter_write()
    print("New Graphic Selected! "..myData.graphicsSelected)
    self:createBuyables() --//!@# potential problem: not being called properly
  end
end

local function closeScene()
    --Generate options
    local options = {
      effect = "fade",
      --This ensures players cannot interact with menu while options are being displayed.
      isModal = true,
      time = 250
    }
    composer.gotoScene("Code.Store.storeBase", options)
end

local function createDemoImage(index)
  local group = display.newGroup()
  local imageOr = display.newImageRect(myData.graphicsLocation[index].."or.png", 36, 36)
  imageOr.anchorX = 0
  imageOr.anchorY = 0
  imageOr.x = -15
  imageOr.y = 15
  local imageAnd = display.newImageRect(myData.graphicsLocation[index].."and.png", 36, 36)
  imageAnd.anchorX = 0
  imageAnd.anchorY = 0
  imageAnd.x = 15
  imageAnd.y = -18
  group:insert(imageOr)
  group:insert(imageAnd)
  return group
end

local function createPrices(index)
  local group = display.newGroup()
  Name = display.newText(group, moneyForCoolLeg[index],0, 0, native.systemFont, 14)
  group:insert(Name)
  return group

end
function scene:createButton(i)
  local fill = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } }
  --TESTPRINT
  print("myData.graphicsBought[1]="..tostring(myData.graphicsBought[1])..".")
  print("myData.graphicsBought[2]="..tostring(myData.graphicsBought[2])..".")
  print("myData.graphicsBought[3]="..tostring(myData.graphicsBought[3])..".")
  if(myData.graphicsBought[i]==true) then
    name = "Select"
    event = self:changeSelect(i)
    if(myData.graphicsSelected == myData.graphicsLocation[i]) then
      name = "Selected"
      fill = { default={ 0.5}, over={ 0.5} }
      event = nil
    end
  else
    name = "Buy"
    event = confirm(i)
    if(myData.coins < moneyForCoolLeg[i]) then
      fill = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } }
      event = nil
    end
  end
  return widget.newButton{
          label = name,
          left = -45,
          id = name..i,
          top = 10,
         onRelease = event,
          --properties for a rounded rectangle button...
         shape="roundedRect",
         width = 90,
         height = 40,
         cornerRadius = 2,
         fillColor = fill,
         labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
         strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
         strokeWidth = 4,
      }
end

--this inserts the 3 items (in own groups)
function scene:createBuyables()
  local name
  local event
  local sceneGroup = scene.view
  for i=1,3,1 do
    items[i] = {}
    items[i].group = display.newGroup()
    items[i].imageGroup = createDemoImage(i)
    items[i].imageGroup.x = -18
    items[i].imageGroup.y = -50
    --this creates th eproperties for the buttons
    items[i].button = self:createButton(i)

    if(myData.graphicsBought[i] ~= true) then
      items[i].costGroup = createPrices(i)
      items[i].costGroup.x = 0
      items[i].costGroup.y = 80
      items[i].group:insert(items[i].costGroup)
    end
    items[i].group:insert(items[i].imageGroup)
    items[i].group:insert(items[i].button)
    items[i].group.x = i * display.contentWidth/4
    items[i].group.y = display.contentCenterY
    sceneGroup:insert(items[i].group)
  end
end
-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   setUpLocations()
   background = display.newImageRect("GFX/Common/title.png", 480, 320)
   background.anchorX = 0
   background.anchorY = 0
   background.x = 0
   background.y = 0
   sceneGroup:insert(background)

   title = display.newText(sceneGroup, "Graphics",100, 30, native.systemFont, 42)
   --before buyables, read inventory
   getResourcesArray()
   --STEP 1] create the buyables
   scene:createBuyables()

       exitButton = widget.newButton{
               label = "Close",
               left = display.contentWidth - 95,
               id = "close",
               top = display.contentHeight - 45,
              onRelease = closeScene,
               --properties for a rounded rectangle button...
              shape="roundedRect",
              width = 90,
              height = 40,
              cornerRadius = 2,
              fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
              labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
              strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
              strokeWidth = 4,
           }
           sceneGroup:insert(exitButton)
   -- Initialize the scene here.
   -- Example: add display objects to "sceneGroup", add touch listeners, etc.
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is still off screen (but is about to come on screen).
      money = display.newText(sceneGroup, "Coins "..tostring(myData.coins), display.contentWidth - 80, 30, native.systemFont, 24)
   elseif ( phase == "did" ) then
      -- Called when the scene is now on screen.
      -- Insert code here to make the scene come alive.
      -- Example: start timers, begin animation, play audio, etc.
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is on screen (but is about to go off screen).
      -- Insert code here to "pause" the scene.
      -- Example: stop timers, stop animation, stop audio, etc.
   elseif ( phase == "did" ) then
   money:removeSelf()
     money = nil
      -- Called immediately after scene goes off screen.
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

   -- Called prior to the removal of scene's view ("sceneGroup").
   -- Insert code here to clean up the scene.
   -- Example: remove display objects, save state, etc.
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
