--audio.lua
--this file will be responsible for providing the string of the current location of the audio to be loaded
--ex. On game start, get the audio from the location specified in the audio.txt file
--------------------------------------------------
local composer = require( "composer" )
local myData = require("Code.mydata")
--------------------------------------------------
--NAME OF THE audio FILE
local nameOfFile="audio.txt"
--Default SFX path
local defaultSFXpath="Go Cart - Loop Mix.mp3"
local customSFXpath="SFX/"



--this function is to be called at the begining of Logical Overflow. It will fetch the location of the audio (SFX location) to use
function getAudioLocation()
  local audioPath

  --First, check if file exists
	local audioLocation =  audioLocationWriter_doesFileExist(nameOfFile, system.DocumentsDirectory)
	if audioLocation then
		--A specific SFX path exists already (possibly for non-default resource). Use that path
    --Read the path from the file
    audioPath = audioLocationWriter_readFile(nameOfFile,system.DocumentsDirectory)
	else
    --A SFX path does not exist (given by lack of audio.txt presence). Re-instantiate default package, using defaultSFXpath
    --write the file with the default path
    print("Creating Audio Text File.")
    audioLocationWriter_writeFile(defaultSFXpath)
    --then use the default path
    audioPath = defaultSFXpath
  end

  --return the audioPath (!@#not used?)
  return audioPath
end

--function to check if audio file exists
function audioLocationWriter_doesFileExist( fname, path )
    local result = false
    -- Path for the file
    local filePath = system.pathForFile( fname, path )
    if ( filePath ) then
        local file, errorString = io.open( filePath, "r" ) --read
        if file then
          result=true
        end
    end
    --return boolean
    return result
end

--function to read audio file
function audioLocationWriter_readFile(fname, path)
  local result = ""
  local filePath = system.pathForFile( fname, path )
    if ( filePath ) then
        local file, errorString = io.open( filePath, "r" ) --read

        if not file then
            -- Error occurred; output the cause
            print( "Audio File Read Error. ")
        else
            -- File exists! Read from file
            if fname == nameOfFile then
              --read one line
              result = file:read("*l") --Reads the line from the current file position, and moves file position to next line.
              --Reset myData.audioSelected
              myData.audioSelected=result
            end
            --print( "Audio File Found: " .. fname.."\n\tValue: "..myData.SFX.location)
        end
        -- Close the file handle
        file:close()
    end
  --return result
  return result

end

--function to write new audio path to audio file.
function audioLocationWriter_writeFile(text)
  local path = system.pathForFile(nameOfFile, system.DocumentsDirectory)
  local file, errorString = io.open( path,  "w+") --All existing data is removed if file exists or new file is created with read write permissions.
  if not file then
    --Error not handled
    --print("File error: " .. errorString)
    file:write(text)
    io.close(file)
  else
    file:write(text)
    io.close(file)
  end
  file = nil

  --read text after write, to update myData
  audioLocationWriter_readFile(nameOfFile,system.DocumentsDirectory)

end

function audioLocationWriter_write()
  audioLocationWriter_writeFile(myData.audioSelected)
end
