local composer = require( "composer" )
local scene = composer.newScene()
local myData = require("Code.mydata")
local widget = require("widget")
local appWarpClient = require "AppWarp.WarpClient"

---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here
local soundEffect = audio.loadSound( "SFX/Common/Menu1.wav" )
local bgm = audio.loadStream("SFX/Music/Go Cart - Loop Mix.mp3")
local background
local chat
local hello
local bye
local greet
local disconnect
local ready
local playerNum
local playerReady
local opponentReady
local rid
local player1ReadyText
local player2ReadyText


---------------------------------------------------------------------------------
--The actual hard work of saying hello!
local function sayHello(event)
  if event.phase == "ended" then
    --TODO I know what to do KappaRoss
    appWarpClient.sendChat("Hello!")
  end
end

--Print Text
function scene.onChatReceived(sender, message)
  chat.text = "Chat: " .. message
end

--Disconnection handling
function scene.onDisconnectDone(resultCode)
  if(resultCode ~= WarpResponseResultCode.SUCCESS) then
    chat.text = "Disconnect failed.."
  else
    chat.text = "Disconnected"
    composer.gotoScene( "Code.Multiplayer.title", "slideLeft", 800  )
  end
end

--Connection handling
function scene.onConnectDone(resultCode)
  if(resultCode ~= WarpResponseResultCode.SUCCESS) then
    chat.text = "Connect failed.."
    composer.gotoScene( "Code.Multiplayer.title", "slideLeft", 800  )
  end
end

--Update room properties handler
function scene.onUpdatePeersReceived(update)
  local func = string.gmatch(update, "%S+")
  local player = tonumber(func())
  local status = func()
  print("status: " .. status)
  if playerNum ~= player then
    if status == "true" then
      opponentReady = true
      player2ReadyText.text = "Opponent: Ready"
    else
      opponentReady = false
      player2ReadyText.text = "Opponent: Not Ready"
    end
  end
  print("PlayerReady: " .. tostring(playerReady))
  print("opponentReady: " .. tostring(opponentReady))
  if playerReady and opponentReady then
    chat.text = "Starting game... Good luck, have fun."
    local options = {
      effect = "fade",
      time = 500,
      params = {
        num = playerNum
      }
    }
    composer.gotoScene("Code.Multiplayer.game", options)
  end
end



--Handle users joining room
function scene.onUserJoinedRoom()
    ready:setEnabled(true)
    ready:setFillColor(0.5, 0.60, 0.5, 1)
    chat.text = "Challenger Approaching! Say Hello!"
end

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   -- Initialize the scene here.
   -- Example: add display objects to "sceneGroup", add touch listeners, etc.
   playerNum = event.params.num
   playerReady = false
   opponentReady = false
   rid = event.params.room
   print(rid)
   local readyButtonStatus
   if playerNum == 1 then
     readyButtonStatus = true
   else
     readyButtonStatus = true
   end

   background = display.newImageRect("GFX/Common/title.png", 480, 320)
   background.anchorX = 0
   background.anchorY = 0
   background.x = 0
   background.y = 0
   sceneGroup:insert(background)

   --Allows user to say "Hello!"
   hello = widget.newButton{
     left = 3*(display.contentWidth)/4-75,
     top = display.contentCenterY-10,
     label = "Say Hello!",
     shape = "roundedRect",
     width = 150,
     height = 50,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = sayHello
   }
   sceneGroup:insert(hello)

   bye = widget.newButton{
     left = 3*(display.contentWidth)/4-75,
     top = display.contentCenterY+45,
     label = "Say Good Bye!",
     shape = "roundedRect",
     width = 150,
     height = 50,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = function(event)
       if event.phase == "ended" then
         --TODO I know what to do KappaRoss
         appWarpClient.sendChat("Good Bye!")
       end
     end
   }
   sceneGroup:insert(bye)

   greet = widget.newButton{
     left = 3*(display.contentWidth)/4-75,
     top = display.contentCenterY+100,
     label = "Greetings!",
     shape = "roundedRect",
     width = 150,
     height = 50,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent =  function(event)
       if event.phase == "ended" then
         --TODO I know what to do KappaRoss
         appWarpClient.sendChat("Greetings!")
       end
     end
   }
   sceneGroup:insert(greet)

   disconnect = widget.newButton{
     left = (display.contentWidth)/4-75,
     top = display.contentCenterY-10,
     label = "Disconnect",
     shape = "roundedRect",
     width = 150,
     height = 50,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = function(event)
       if "ended" == event.phase then
         chat.text = "disconnecting.."
         appWarpClient.sendChat("Opponent has quit.")
         appWarpClient.disconnect()
        end
      end
   }
   sceneGroup:insert(disconnect)

   ready = widget.newButton{
     left = (display.contentWidth)/4-75,
     top = display.contentCenterY+45,
     label = "Ready",
     shape = "roundedRect",
     width = 150,
     height = 50,
     isEnabled = readyButtonStatus,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = function(event)
       if "ended" == event.phase then
         playerReady = not playerReady
         if playerReady then
           player1ReadyText.text = "Player: Ready"
         else
           player1ReadyText.text = "Player: Not Ready"
         end
         appWarpClient.sendUpdatePeers(tostring(playerNum) .. " " .. tostring(playerReady))
        end
      end
   }
   sceneGroup:insert(ready)
   if playerNum == 1 then
     ready:setEnabled(false)
     ready:setFillColor(0.5)
   end
   chat = display.newText(sceneGroup, "Have a friendly chat!", display.contentCenterX, display.contentCenterY-100, native.systemFont, 20)
   player1ReadyText = display.newText(sceneGroup, "Player: Not Ready", (display.contentWidth)/4, display.contentCenterY+110, native.systemFont, 12)
   player2ReadyText = display.newText(sceneGroup, "Opponent: Not Ready", (display.contentWidth)/4, display.contentCenterY+125, native.systemFont, 12)
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is still off screen (but is about to come on screen).
      playerReady = false
      opponentReady = false
      player1ReadyText.text = "Player: Not Ready"
      player2ReadyText.text = "Opponent: Not Ready"

   elseif ( phase == "did" ) then
      --composer.removeHidden()
      appWarpClient.addRequestListener("onConnectDone", scene.onConnectDone)
      appWarpClient.addRequestListener("onDisconnectDone", scene.onDisconnectDone)
      appWarpClient.addNotificationListener("onChatReceived", scene.onChatReceived)
      appWarpClient.addNotificationListener("onUpdatePeersReceived", scene.onUpdatePeersReceived)
      appWarpClient.addNotificationListener("onUserJoinedRoom", scene.onUserJoinedRoom)
      composer.removeScene("Code.Multiplayer.game")
      audio.play(bgm, {channel=1, loops=-1})
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
      -- Called immediately after scene goes off screen.
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view
   --Do as Barney does and clean up.

end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


---------------------------------------------------------------------------------

return scene
