local composer = require( "composer" )
local scene = composer.newScene()
local myData = require("Code.mydata")
local widget = require("widget")
local physics = require( "physics" )
local appWarpClient = require "AppWarp.WarpClient"
---------------------------------------------------------------------------------

-- local forward references should go here
local bg
local question
local qText
local numObjects
local staticObjects
local dynamicObjects
local temp
local player1
local player2
local playerNum
local isDragAllowed = false
local p1score
local p2score
local curQ
local spawnTime
local win
local delay = false

---------------------------------------------------------------------------------
--Generate Questions
local function loadQ()
	question[1] = "if( [? 1 ?](A [? 2 ?] B) )"
	question["1A"] = "A = false, "
	question["1B"] = "B = false"
	question["1ans1"] = "not"
	question["1ans2"] = "or"

	question[2] = "if( [? 2 ?](A) )"
	question["2A"] = "A = [? 1 ?], "
	question["2B"] = "B = null"
	question["2ans1"] = "false"
	question["2ans2"] = "not"

	question[3] = "if( [? 1 ?](A && B) [? 2 ?] false)"
	question["3A"] = "A = true, "
	question["3B"] = "B = false"
	question["3ans1"] = "not"
	question["3ans2"] = "or"

	question[4] = "if( (A [? 1 ?] B) && !(C [? 2 ?] D))"
	question["4A"] = "A = true, B = false "
	question["4B"] = "C = false, D = false"
	question["4ans1"] = "or"
	question["4ans2"] = "and"

	question[5] = "if( !(A && B)  && [? 2 ?](C && D))"
	question["5A"] = "A = [? 1 ?], B = false "
	question["5B"] = "C = false, D = false"
	question["5ans1"] = "false"
	question["5ans2"] = "not"
end

--make a new quesstion
local function newQuestion()
	local oldQ = curQ
	while oldQ == curQ do
		curQ = math.random(1, 5)
	end
	qText.text = question[curQ.."A"]..question[curQ.."B"].."\n"..question[curQ]
	appWarpClient.sendUpdatePeers(tostring(playerNum) .. " " .. "qchange" .. " " .. tostring(curQ))
end

--Return to lobby
local function toLobby(event)
	local options = {
		effect = "fade",
		time = 1000
	}
	composer.gotoScene("Code.Multiplayer.chatScene", options)
end

---check win
local function checkWin(event)
	if p1score == win then
		if playerNum == 1 then
			qText.text = "You won!"
		else
			qText.text = "You're opponent has won!"
		end
		Runtime:removeEventListener(	"enterFrame", randoSpawn)
		timer.performWithDelay(1000, toLobby())
	elseif p2score == win then
		if playerNum == 2 then
			qText.text = "You won!"
		else
			qText.text = "You're opponent has won!"
		end
		Runtime:removeEventListener(	"enterFrame", randoSpawn)
		timer.performWithDelay(1000, toLobby())
	end
end

--Check answers
local function checkAnswer(event)

		if question[tostring(curQ).."ans1"] == staticObjects[1].type and question[tostring(curQ).."ans2"] == staticObjects[2].type then
			if playerNum == 1 then
				p1score = p1score + 1
				player1.text = "My Score: " .. p1score
			else
				p1score = p1score + 1
				player1.text = "Opponent Score: " .. p1score
			end
			appWarpClient.sendUpdatePeers(tostring(playerNum) .. " " .. "score" .. " " .. tostring(p1score) .. " " .. tostring(p2score))
			staticObjects[1].type = 0
			staticObjects[2].type = 0
			display.remove(dynamicObjects[staticObjects[1].hold])
			display.remove(dynamicObjects[staticObjects[2].hold])
			staticObjects[1].hold = 0
			staticObjects[2].hold = 0
			spawnTime = system.getTimer()
			numObjects = numObjects - 2
			if p1score ~= win then
				if playerNum == 1 then
					newQuestion()
				end
			end
		end

		if question[tostring(curQ).."ans1"] == staticObjects[3].type and question[tostring(curQ).."ans2"] == staticObjects[4].type then
			if playerNum == 1 then
				p2score = p2score + 1
				player2.text = "Opponent's Score: " .. p2score
			else
				p2score = p2score + 1
				player2.text = "My Score: " .. p2score
			end
			appWarpClient.sendUpdatePeers(tostring(playerNum) .. " " .. "score" ..  " " .. tostring(p1score) .. " " .. tostring(p2score))
			staticObjects[3].type = 0
			staticObjects[4].type = 0
			display.remove(dynamicObjects[staticObjects[3].hold])
			display.remove(dynamicObjects[staticObjects[4].hold])
			staticObjects[3].hold = 0
			staticObjects[4].hold = 0
			spawnTime = system.getTimer()
			numObjects = numObjects - 2
			if p2score ~= win then
				if playerNum == 1 then
					newQuestion()
				end
			end
		end

end

--Set locations
local function setLocs(event, obj1, obj2)
    obj1.x = obj2.x + 25
    obj1.y = obj2.y + 25
    isDragAllowed = false
end

--Handle collisions
local function onLocalCollision(self, event)
		if event.phase == "began" then
			if self.type == 0 then
				self.type = event.other.type
				local mycol = function()return setLocs(event, event.other, self) end
				timer.performWithDelay(100, mycol, 1)
				self.hold = event.other.index
				checkAnswer()
			elseif self.type == "garbage" then
				display.remove(event.other)
				numObjects = numObjects - 1
			end
		elseif event.phase == "ended" and self.type == event.other.type then
			self.type = 0
			self.hold = nil
		end
		return true
end

--Handle drag and drop
local function startDrag(event)
	local t = event.target

	local phase = event.phase
	if "began" == phase then
		-- Make target the top-most object
		local parent = t.parent
		parent:insert( t )
		display.getCurrentStage():setFocus( t )
    isDragAllowed = true
		-- Spurious events can be sent to the target, e.g. the user presses
		-- elsewhere on the screen and then moves the finger over the target.
		-- To prevent this, we add this flag. Only when it's true will "move"
		-- events be sent to the target.
		t.isFocus = true

		-- Store initial position
		t.x0 = event.x - t.x
		t.y0 = event.y - t.y
	elseif t.isFocus then
		if "moved" == phase and isDragAllowed then
			-- Make object move (we subtract t.x0,t.y0 so that moves are
			-- relative to initial grab point, rather than object "snapping").
			t.x = event.x - t.x0
			t.y = event.y - t.y0

		elseif "ended" == phase or "cancelled" == phase or not isDragAllowed then
			display.getCurrentStage():setFocus( nil )
			t.isFocus = false
      isDragAllowed = false
		end
		appWarpClient.sendUpdatePeers(tostring(playerNum) .. " " .. "move" .. " " .. tostring(t.index) .. " " .. tostring(t.x) .. " " .. tostring(t.y))
	end

	-- Important to return true. This tells the system that the event
	-- should not be propagated to listeners of any objects underneath.
	return true
end

--Spawning objects
local function spawn(params)
  local object = display.newImageRect(params.img, 50, 50)
  object.anchorX = params.ax or 0.5
  object.anchorY = params.ay or 0.5
  object.x = params.x
  object.y = params.y

  object.objTable = params.table or dynamicObjects
  --Automatically set the table index to be inserted into the next available table index
  object.index = #object.objTable + 1

  --Give the object a custom name
  object.myName = "Object : " .. object.index

  object.colprev = params.colprev
	object.type = params.type

  if params.hasBody then
    --Allow physics parameters to be passed by parameters:
    object.density = params.density or 0
    object.friction = params.friction or 0
    object.bounce = params.bounce or 0
    object.isSensor = params.isSensor or false
    object.bodyType = params.bodyType or "dynamic"
    object.hasFilter = {groupIndex=params.filter}  or {groupIndex=0}
    object.collision = onLocalCollision
    object:addEventListener("collision", object)
    physics.addBody(object, object.bodyType, {density = object.density, friction = object.friction, bounce = object.bounce, isSensor = object.isSensor, filter=object.hasFilter})
  end

  --The objects group
  object.group = params.group or nil

  --If the function call has a parameter named group then insert it into the specified group
  object.group:insert(object)
	object.isVisible = params.visible or true
  --Insert the object into the table at the specified index
  object.objTable[object.index] = object
  if params.drag then
    object:addEventListener("touch", startDrag)
  end

  return object
end

--Peer update handler
function scene.onUpdatePeersReceived(update)
		local func = string.gmatch(update, "%S+")
		local who = tonumber(func())
		local flow = func()
		if flow ~= "move" then
			print(flow)
		end
		if flow == "move" and who ~= playerNum then
			local params = {}
			params.id = tonumber(func())
			params.x = tonumber(func())
			params.y = tonumber(func())
			peerMove(params)
		elseif flow == "score" then
			print("in flow: score")
			local lp1score = tonumber(func())
			local lp2score = tonumber(func())
			p1score = lp1score
			p2score = lp2score
			if playerNum == 1 then
				player1.text = "My Score: " .. p1score
				player2.text = "Opponent's Score: " .. p2score
			else
				player1.text = "Opponent's Score: " .. p1score
				player2.text = "My Score: " .. p2score
			end
			for i, Value in pairs( dynamicObjects ) do
				if dynamicObjects[i] then
					display.remove(dynamicObjects[i])
					dynamicObjects[i] = nil
				end
			end
			for i = 1, 4, 1 do
				staticObjects[i].type = 0
				staticObjects[i].hold = 0
			end
			numObjects = 0
			checkWin()
		elseif flow == "spawn" and who ~= playerNum then
			func()
			local rx = func()
			local ry = func()
			local rtype = func()
			spawn({
				img = "GFX/Multiplayer/" .. rtype .. ".png",
				x = rx,
				y = ry,
				group  = staticObjects[1].group,
				hasBody = true,
				drag = true,
				filter = -1,
				type = rtype,
			})
		elseif flow == "qchange" and who ~= playerNum then
			curQ = tonumber(func())
			qText.text = question[curQ.."A"]..question[curQ.."B"].."\n"..question[curQ]
		elseif flow == "snap" and who ~= playerNum then
			print("snap")
			local i = tonumber(func())
			local hold = tonumber(func())
			staticObjects[i].hold = hold
			dynamicObjects[hold].x = staticObjects[i].x
			dynamicObjects[hold].y = staticObjects[i].y
		end
end

--Handle peer move
function peerMove(params)
		dynamicObjects[params.id].x = params.x
		dynamicObjects[params.id].y = params.y
end

function randoSpawn()
	if (system.getTimer() - spawnTime) >= 1000 and playerNum == 1 and numObjects < 10 then
			local choice = math.random(1, 5)
			if choice == 1 then
				--spawn an or
				local i = spawn({
	        img = "GFX/Multiplayer/or.png",
	        x = math.random(display.contentWidth/4-55, 3*display.contentWidth/4+50),
	        y = math.random(100, display.contentHeight-100),
	        group  = staticObjects[1].group,
	        hasBody = true,
	        drag = true,
	        filter = -1,
	 			 	type = "or",
	      })
	 			appWarpClient.sendUpdatePeers(tostring(playerNum) .. " " .. "spawn" .. " " .. tostring(i.index) .. " " .. tostring(i.x) .. " " .. tostring(i.y) .. " " .. "or")
			elseif choice == 2 then
				--spawn an and
				local i = spawn({
	        img = "GFX/Multiplayer/and.png",
	        x = math.random(display.contentWidth/4-55, 3*display.contentWidth/4+50),
	        y = math.random(100, display.contentHeight-100),
	        group  = staticObjects[1].group,
	        hasBody = true,
	        drag = true,
	        filter = -1,
	 			 	type = "and",
	      })
	 			appWarpClient.sendUpdatePeers(tostring(playerNum) .. " " .. "spawn" .. " " .. tostring(i.index) .. " " .. tostring(i.x) .. " " .. tostring(i.y) .. " " .. "and")
			elseif choice == 3 then
				--spawn a not
				local i = spawn({
	        img = "GFX/Multiplayer/not.png",
	        x = math.random(display.contentWidth/4-55, 3*display.contentWidth/4+50),
	        y = math.random(100, display.contentHeight-100),
	        group  = staticObjects[1].group,
	        hasBody = true,
	        drag = true,
	        filter = -1,
	 			 	type = "not",
	      })
	 			appWarpClient.sendUpdatePeers(tostring(playerNum) .. " " .. "spawn" .. " " .. tostring(i.index) .. " " .. tostring(i.x) .. " " .. tostring(i.y) .. " " .. "not")
			elseif choice == 4 then
				--spawn a true
				local i = spawn({
	        img = "GFX/Multiplayer/true.png",
	        x = math.random(display.contentWidth/4-55, 3*display.contentWidth/4+50),
	        y = math.random(100, display.contentHeight-100),
	        group  = staticObjects[1].group,
	        hasBody = true,
	        drag = true,
	        filter = -1,
	 			 	type = "true",
	      })
	 			appWarpClient.sendUpdatePeers(tostring(playerNum) .. " " .. "spawn" .. " " .. tostring(i.index) .. " " .. tostring(i.x) .. " " .. tostring(i.y) .. " " .. "true")
			elseif choice == 5 then
				--spawn a false
				local i = spawn({
	        img = "GFX/Multiplayer/false.png",
	        x = math.random(display.contentWidth/4-55, 3*display.contentWidth/4+50),
	        y = math.random(100, display.contentHeight-100),
	        group  = staticObjects[1].group,
	        hasBody = true,
	        drag = true,
	        filter = -1,
	 			 	type = "false",
	      })
	 			appWarpClient.sendUpdatePeers(tostring(playerNum) .. " " .. "spawn" .. " " .. tostring(i.index) .. " " .. tostring(i.x) .. " " .. tostring(i.y) .. " " .. "false")
			end
			spawnTime = system.getTimer()
			numObjects = numObjects + 1
	elseif (system.getTimer() - spawnTime) >= 500 and playerNum == 1 and numObjects >= 10 then
		spawnTime = system.getTimer()
	end
end

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   playerNum = event.params.num
	 p1score = 0
	 p2score = 0
	 numObjects = 0
	 spawnTime = system.getTimer() + 5000
	 win = event.params.win or 3
   physics.start()
   physics.setDrawMode( "normal" )
   physics.setGravity( 0, 0 )
   bg = display.newImageRect("GFX/Common/whitebg.png", 480, 320)
   bg.anchorX = 0
   bg.anchorY = 0
   bg.x = 0
   bg.y = 0
   sceneGroup:insert(bg)

   question = {}
   staticObjects = {}
   dynamicObjects = {}

	 curQ = 1
	 loadQ()

   local options = {
     parent = sceneGroup,
     text = question["1A"]..question["1B"].."\n"..question[1],
     x = display.contentCenterX,
     y = 50,
     width = 240,
     font = native.systemFont,
     fontSize = 16,
     align = "center"
   }
   qText = display.newText(options)
   qText:setTextColor(0,0,0)

   options = {
     parent = sceneGroup,
     text = "My Score: 0",
     x = display.contentWidth/4,
     y = display.contentHeight-65,
     width = 240,
     font = native.systemFont,
     fontSize = 12,
     align = "center"
   }
   player1 = display.newText(options)
   player1:setTextColor(0,0,0)

   options = {
     parent = sceneGroup,
     text = "Opponent's Score: 0",
     x = 3*display.contentWidth/4,
     y = display.contentHeight-65,
     width = 240,
     font = native.systemFont,
     fontSize = 12,
     align = "center"
   }
   player2 = display.newText(options)
   player2:setTextColor(0,0,0)

	 if playerNum == 2 then
		 player2.text = "My Score: " .. p1score
		 player1.text = "Opponent's Score: " .. p2score
	 end

   local params = {
     img = "GFX/Multiplayer/unknown.png",
     ax = 0,
     ay = 0,
     x = display.contentWidth/4 - 55,
     y = display.contentHeight - 55,
     table = staticObjects,
     group = sceneGroup,
     hasBody = true,
     isSensor = true,
     bodyType = "static",
     colprev = true,
		 type = 0
   }
   spawn(params)

   params = {
     img = "GFX/Multiplayer/unknown.png",
     ax = 0,
     ay = 0,
     x = display.contentWidth/4,
     y = display.contentHeight - 55,
     table = staticObjects,
     group = sceneGroup,
     hasBody = true,
     isSensor = true,
     bodyType = "static",
     colprev = true,
		 type = 0
   }
   spawn(params)

   params = {
     img = "GFX/Multiplayer/unknown.png",
     ax = 0,
     ay = 0,
     x = 3*display.contentWidth/4 - 55,
     y = display.contentHeight - 55,
     table = staticObjects,
     group = sceneGroup,
     hasBody = true,
     isSensor = true,
     bodyType = "static",
     colprev = true,
		 type = 0
   }
   spawn(params)

   params = {
     img = "GFX/Multiplayer/unknown.png",
     ax = 0,
     ay = 0,
     x = 3*display.contentWidth/4,
     y = display.contentHeight - 55,
     table = staticObjects,
     group = sceneGroup,
     hasBody = true,
     isSensor = true,
     bodyType = "static",
     colprev = true,
		 type = 0
   }
   spawn(params)

	 params = {
     img = "GFX/Multiplayer/garbage.png",
     ax = 0,
     ay = 0,
     x = (display.contentWidth/4 + 3*display.contentWidth/4 - 55)/2,
     y = display.contentHeight - 55,
     table = staticObjects,
     group = sceneGroup,
     hasBody = true,
     isSensor = true,
     bodyType = "static",
     colprev = true,
		 type = "garbage"
   }
   spawn(params)
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
		 appWarpClient.addNotificationListener("onUpdatePeersReceived", scene.onUpdatePeersReceived)
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then

   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

end



---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
Runtime:addEventListener(	"enterFrame", randoSpawn)


---------------------------------------------------------------------------------

return scene
