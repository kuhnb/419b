local composer = require( "composer" )
local scene = composer.newScene()
local myData = require("Code.mydata")
local widget = require("widget")
local appWarpClient = require "AppWarp.WarpClient"

---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here
local path = "GFX/Multiplayer/tutorial"
local img1
local img2
local img3
local img4
local img5
local text


---------------------------------------------------------------------------------
local function toTitle(event)
  if event.phase == 'ended' then
    composer.gotoScene("Code.Multiplayer.title", {effect="slideLeft", time=500})
  end
end

local function tutListener(event)
  if event.phase == "ended" then
    if (event.target.id == 1) then
      text.isVisible = false
      img1.isVisible = false
      img2.isVisible = true
    elseif event.target.id == 2 then
      img2.isVisible = false
      img3.isVisible = true
    elseif event.target.id == 3 then
      img3.isVisible = false
      img4.isVisible = true
    elseif event.target.id == 4 then
      img4.isVisible = false
      img5.isVisible = true
    else
      toTitle(event)
    end
  end
end
-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   -- Initialize the scene here.
   img1 = display.newImageRect(path .. "/tut1.png", 480, 320)
   img1.x = display.contentCenterX
   img1.y = display.contentCenterY
   img1.id = 1
   sceneGroup:insert(img1)

   img2 = display.newImageRect(path .. "/tut2.png", 480, 320)
   img2.x = display.contentCenterX
   img2.y = display.contentCenterY
   img2.id = 2
   sceneGroup:insert(img2)
   img2.isVisible = false

   img3 = display.newImageRect(path .. "/tut3.png", 480, 320)
   img3.x = display.contentCenterX
   img3.y = display.contentCenterY
   img3.id = 3
   sceneGroup:insert(img3)
   img3.isVisible = false

   img4 = display.newImageRect(path .. "/tut4.png", 480, 320)
   img4.x = display.contentCenterX
   img4.y = display.contentCenterY
   img4.id = 4
   sceneGroup:insert(img4)
   img4.isVisible = false

   img5 = display.newImageRect(path .. "/tut5.png", 480, 320)
   img5.x = display.contentCenterX
   img5.y = display.contentCenterY
   img5.id = 5
   sceneGroup:insert(img5)
   img5.isVisible = false

   options = {
     parent = sceneGroup,
     text = "Tap anywhere to continue",
     x = display.contentWidth-75,
     y = 15,
     font = native.systemFont,
     fontSize = 12,
     align = "center"
   }
   text = display.newText(options)
   text:setTextColor(1,0,0)
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is still off screen (but is about to come on screen).
   elseif ( phase == "did" ) then
      --composer.removeHidden()
      img1:addEventListener("touch", tutListener)
      img2:addEventListener("touch", tutListener)
      img3:addEventListener("touch", tutListener)
      img4:addEventListener("touch", tutListener)
      img5:addEventListener("touch", tutListener)
      audio.play(bgm, {channel=1, loops=-1})
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
      -- Called immediately after scene goes off screen.
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view
   --Do as Barney does and clean up.

end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


---------------------------------------------------------------------------------

return scene
