local composer = require( "composer" )
local scene = composer.newScene()
local myData = require("Code.mydata")
local widget = require("widget")
local appWarpClient = require "AppWarp.WarpClient"


---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here
local background
local title
local goal
local soundEffect = audio.loadSound( "SFX/Common/Menu1.wav" )
local bgm = audio.loadStream("SFX/Music/Go Cart - Loop Mix.mp3")
local optionsButton
local tutorial
local play
local isNewRoomCreated
local playerNum
local rid
local roomText
local roomRect
local roomJoinText
local create
local connect
local type
local opti
local backi


appWarpClient.initialize("98e56f3d0cdb35fe9009358403fa4fa26ced732203a00699a6ae87e7262f23ed", "3873a6d9de6306fb8525d0c7987caedd99a81b053118df9ed0e78c4ff59cd8f7")

local function gameLoop(event)
  appWarpClient.Loop()
end
if not myData.loopadded then
  myData.loopadded = true
  print("gameLoop added")
  Runtime:addEventListener("enterFrame", gameLoop)
end
---------------------------------------------------------------------------------


--Open options overlay
local function openOptions(event)

  --Generate options
  local options = {
    effect = "fade",
    --This ensures players cannot interact with menu while options are being displayed.
    isModal = true,
    time = 500,
    params = {
      text1 = "Close Options"
    }
  }
  --Show the options overlay.
  if event.phase == "ended" then
    composer.showOverlay("Code.options", options)
  end
end

local function startTutorial(event)
  if event.phase == "ended" then
    composer.gotoScene("Code.multiplayer.tutorial", {effect="fade", time=500})
  end
end

--Return to select game screen. So the player can play the good game.
local function toSelect(event)
  --Generate options.
  local options = {
    effect = "fade",
    time = 1000
  }
  --Go to gameSelect.lua
  if event.phase == "ended" then
    myData.loopadded = false
    Runtime:removeEventListener("enterFrame", gameLoop)

    audio.play(soundEffect, {channel=2})
    composer.gotoScene("Code.gameSelect", options)
  end
end

local function createLobby(event)
  type = "create"
end

--connect via code
local function connectToLobby(event)
  if(event.phase == "ended") then
    type = "connect"
    if (rid) then
      appWarpClient.joinRoom(roomid)
    else
      roomText.isVisible = true
      roomJoinText.isVisible = true
      roomRect.isVisible = true
    end
  else
    goal.text = "Error: Incorrect Room Id"
  end
end



--Start connecting
local function startConnect(event)
  if event.phase == "ended" then
    goal.fontSize = 20
    goal.text = "Connecting.."
    appWarpClient.connectWithUserName(tostring(os.clock()))
  end
end

local function inputComplete(event)
  if event.phase == "ended" or event.phase == "submitted" then
    rid = event.target.text
    roomText.isVisible = false
    roomJoinText.isVisible = false
    roomRect.isVisible = false
    startConnect(event)
  end
end

--Listener for when connecting is done
local function onConnectDone(resultCode)
  if(resultCode == WarpResponseResultCode.SUCCESS) then

    if not type then type = "quick" end
    if type == "quick" then
      appWarpClient.joinRoomInRange(1, 1, false)
    elseif type == "connect" then
      appWarpClient.joinRoom(rid)
    elseif type == "create" then
      local roomPropertiesTable = {}
      roomPropertiesTable["result"] = ""
      ROOM_ADMIN = "admin"
      isNewRoomCreated = true
      appWarpClient.createRoom("Logical.Multiplayer", ROOM_ADMIN, 2, roomPropertiesTable, 30)
    end
  else
    goal.text = "Connect failed.."
  end
end

--Listener for when user connects to room
local function onJoinRoomDone(resultCode, roomid)
  if(resultCode == WarpResponseResultCode.SUCCESS) then
    if not playerNum then playerNum = 2 end
    rid = roomid
    print(rid)
    appWarpClient.subscribeRoom(roomid)
  elseif (resultCode==WarpResponseResultCode.RESOURCE_NOT_FOUND and type == "quick") then
    --no room found, creating a new one
    playerNum = 1
    local roomPropertiesTable = {}
    roomPropertiesTable["result"] = ""
    ROOM_ADMIN = "admin"
    isNewRoomCreated = true
    appWarpClient.createRoom("Logical.Multiplayer", ROOM_ADMIN, 2, roomPropertiesTable, 30)
  else
    goal.text = "Join Room Failed. Check Room Id."
  end
end

local function onCreateRoomDone(resultCode, roomid, name)
  if(resultCode == WarpResponseResultCode.SUCCESS) then
    if type == "create" then
      --stuff to invie here
    end
    appWarpClient.joinRoom(roomid)
  else
    goal.text = "Room creation failed."
  end
end

--Listener for when user subscribes to a room
local function onSubscribeRoomDone(resultCode)
  if(resultCode == WarpResponseResultCode.SUCCESS) then
    local options = {
      effect = "slideLeft",
      time = 500,
      params = {
        num = playerNum,
        room = rid
      }
    }
    composer.gotoScene( "Code.Multiplayer.chatScene", options)
  else
    goal.text = "subscribeRoom failed"
  end
end


-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   -- Initialize the scene here.
   -- Example: add display objects to "sceneGroup", add touch listeners, etc.
   isNewRoomCreated = false

   background = display.newImageRect("GFX/Common/title.png", 480, 320)
   background.anchorX = 0
   background.anchorY = 0
   background.x = 0
   background.y = 0
   sceneGroup:insert(background)

   --Generate and set text
   title = display.newText(sceneGroup, "Logical.Multiplayer", display.contentCenterX, 40, native.systemFont, 42)
   goal = display.newText(sceneGroup, "Welcome!", display.contentCenterX, 100, native.systemFont, 20)
   goal:setTextColor(0,0,0)

   --Create connect button
   play = widget.newButton{
     left = (display.contentWidth-200)/2-100,
     top = display.contentCenterY-25,
     label = "Connect & Play",
     shape = "roundedRect",
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = startConnect
   }
   sceneGroup:insert(play)

   --Create connect button
   create = widget.newButton{
     left = (display.contentWidth-200)/2+100,
     top = display.contentCenterY-25,
     label = "Create & Invite",
     shape = "roundedRect",
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = createLobby
   }
   create:setEnabled(false)
   create:setFillColor(0.5)
   sceneGroup:insert(create)

   --Create connect button
   connect = widget.newButton{
     left = (display.contentWidth-200)/2+100,
     top = display.contentCenterY+35,
     label = "Connect",
     shape = "roundedRect",
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = connectToLobby
   }
   connect:setEnabled(false)
   connect:setFillColor(0.5)
   sceneGroup:insert(connect)

   --Create connect button
   tut = widget.newButton{
     left = (display.contentWidth-200)/2-100,
     top = display.contentCenterY+35,
     label = "Tutorial",
     shape = "roundedRect",
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = startTutorial
   }
   sceneGroup:insert(tut)

   --Create and set the back to game select button image.

   backButton = widget.newButton{
     left = display.contentWidth - 43,
     top = display.contentHeight - 43,
     shape = "roundedRect",
     font = native.systemFont,
     width = 40,
     height = 40,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = toSelect
   }
   sceneGroup:insert(backButton)

   backi = display.newImageRect("GFX/Common/left224.png", 35, 35)
   backi.x = backButton.x
   backi.y = backButton.y
   sceneGroup:insert(backi)

   --Create and set an options button
   --Create and set an options button
   optionsButton = widget.newButton{
     left = 0,
     top = display.contentHeight-43,
     shape = "roundedRect",
     font = native.systemFont,
     width = 40,
     height = 40,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = openOptions
   }
   sceneGroup:insert(optionsButton)

   opti = display.newImageRect("GFX/Common/gear74.png", 35, 35)
   opti.x = optionsButton.x
   opti.y = optionsButton.y
   sceneGroup:insert(opti)




   roomRect = display.newRoundedRect(sceneGroup, display.contentCenterX, display.contentCenterY, 240, 180, 4)
   roomRect.strokeWidth = 3
   roomRect:setFillColor( 0.5, 0.60, 0.5 )
   roomRect:setStrokeColor( 0, 0, 0 )
   roomRect.isVisible = false

   options = {
     parent = sceneGroup,
     text = "Enter Room Id",
     x = display.contentCenterX,
     y = display.contentCenterY-65,
     width = 240,
     font = native.systemFont,
     fontSize = 12,
     align = "center"
   }
   roomJoinText = display.newText(options)
   roomJoinText:setTextColor(0,0,0)
   roomJoinText.isVisible = false

   roomText = native.newTextField(display.contentCenterX, display.contentCenterY, 150, 50)
   roomText.isVisible = false
   sceneGroup:insert(roomText)

   roomText.inputType = "number"

end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is still off screen (but is about to come on screen).
   elseif ( phase == "did" ) then
      --composer.removeHidden()
      roomText:addEventListener("userInput", inputComplete)
      appWarpClient.addRequestListener("onConnectDone", onConnectDone)
      appWarpClient.addRequestListener("onJoinRoomDone", onJoinRoomDone)
      appWarpClient.addRequestListener("onSubscribeRoomDone", onSubscribeRoomDone)
      appWarpClient.addRequestListener("onCreateRoomDone", onCreateRoomDone)


      audio.play(bgm, {channel=1, loops=-1})
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is on screen (but is about to go off screen).
      -- Insert code here to "pause" the scene.
      -- Example: stop timers, stop animation, stop audio, etc.
      roomText.isVisible = false


   elseif ( phase == "did" ) then
      -- Called immediately after scene goes off screen.
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view
   --Do as Barney does and clean up.
   optionsButton:removeSelf()
   optionsButton = nil
   backButton:removeSelf()
   backButton = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


---------------------------------------------------------------------------------

return scene
