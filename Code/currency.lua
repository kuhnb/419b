--currency.lua
--this file will be responsible for handling all (soft) currecny functions/transactions
--ex. Incrementing/Decrementing "coins".
--------------------------------------------------
local composer = require( "composer" )
local myData = require("Code.mydata")

--------------------------------------------------

--this function is to be called at the begining of Logical Overflow.
--It will read (if present) the user's last currency value, and load it into myData
function initializeCurrency()
	--check if coins.txt exists
	local hasPreviousCoins = coins_doesFileExist("coins.txt", system.DocumentsDirectory)
	--if it hasn't, write it with 0 coins
	if not hasPreviousCoins then
    coins_writeToFile("0")
	end
end

--function to check if coins.txt exists
--And if so, read from it to myData
function coins_doesFileExist( fname, path )
	print("file exist")
    local results = false
    -- Path for the file
    local filePath = system.pathForFile( fname, path )
    if ( filePath ) then
        local file, errorString = io.open( filePath, "r" )

        if not file then
            -- Error occurred; output the cause
            print( "Currecny File Error: " .. errorString )
        else
            -- File exists! Read from coins.txt
            print( "Currency File Found: " .. fname )
            local contents = file:read("*n") --reads from current file position and returns a number if exists at the file position or returns null
            if fname == "coins.txt" then
              myData.coins = contents
              print("User's Coins: " .. tostring(myData.coins))
            end
            results = true
            -- Close the file handle
            file:close()
        end
    end
    return results
end

--function to write the user's coins value to coins.txt
function coins_writeToFile(value)
  local path = system.pathForFile("coins.txt", system.DocumentsDirectory)
  local file, errorString = io.open( path,  "w+") --all existing data is removed if file exists or new file is crated with reaad write perms
  local text = value
  if not file then
    --print("File error: " .. errorString)
    file:write(text)
    io.close(file)
  else
    file:write(text)
    io.close(file)
  end
	myData.coins = text
  file = nil
end

--This function is user to change the users coins.
--(ie. Method for transacting user's coins)
function changeUserCoins(delta)
	local newCoinValue = myData.coins+delta
	myData.coins=newCoinValue
	coins_writeToFile(newCoinValue)
	print("$$ Coins Changed, from "..(myData.coins-delta).." to "..myData.coins..".")
end

--function to check if transaction is valid, given transaction value
function isTranactionOkay(transactionValue)
	transactionValue=math.abs(transactionValue)
	local newCoins = myData.coins-transactionValue
	if newCoins>=0 then
		--allow
		print("$$  Tranaction Acceptable")
		changeUserCoins(-1*transactionValue)
		return true
	else
		--deny
		print("$$  Tranaction Denied")
		return false
	end
end

-----------------TEST METHODS----------------

function testIncrementDecrement()
	--test increment and decrement
	print("Testing Coins Increment/Decrement")
	changeUserCoins(33) --increment
	changeUserCoins(-33) --decrement
end

----------------------------------------------
