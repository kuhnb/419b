local composer = require( "composer" )
local scene = composer.newScene()
local myData = require("Code.mydata")
---------------------------------------------------------------------------------

-- local forward references should go here
local consent
local displayed
local parse = require("mod_parse")
---------------------------------------------------------------------------------

--Save the parse object id for later use
local function saveLTId(event)
  if not event.error then
    myData.ltid = event.response.objectId
  end
end

--Save the parse object id for later use
local function saveNMId(event)
  if not event.error then
    myData.nmid = event.response.objectId
  end
end

--Save Combo id
local function saveCId(event)
  if not event.error then
    myData.cid = event.response.objectId
  end
end

--Save LevelPlay id
local function saveLPId(event)
  if not event.error then
    myData.lpid = event.response.objectId
  end
end

--Save NumBoopl id
local function saveNBPLId(event)
  if not event.error then
    myData.nbplid = event.response.objectId
  end
end

--Save SwitchVal id
local function saveSVId(event)
  if not event.error then
    myData.svid = event.response.objectId
  end
end





-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   local datatable = {}
   --Generate parse object if consent was given
   if myData.consent then
     print("hello")
     parse:createObject("LevelTime", datatable, saveLTId)
     parse:createObject("NumMistake", datatable, saveNMId)
     parse:createObject("Combo", datatable, saveCId)
     parse:createObject("LevelPlay", datatable, saveLPId)
     parse:createObject("NumBoopl", datatable, saveNBPLId)
     parse:createObject("SwitchVal", datatable, saveSVId)
   else
     print("Consent was refused. Go to gameselect.")
   end
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
      --Pasre Setup Completed. Need to go to the scene that allows player to put in alias (//!@# come back and modify goTo)
      composer.gotoScene("Code.gameSelect")
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then

   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


---------------------------------------------------------------------------------

return scene
