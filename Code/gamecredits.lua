local composer = require( "composer" )
local scene = composer.newScene()

---------------------------------------------------------------------------------

-- local forward references should go here
local credits
local shame = "Brandon Kuhn     Sam Miller     Barret Sharpe     Arthur Lam"
local shame2
local music1
local music2
local icon1
local icon2
local h1
local h2
local h3
local text1 = "\"Daily Beetle\" Kevin MacLeod (incompetech.com) \nLicensed under Creative Commons: By Attribution 3.0 \nhttp://creativecommons.org/licenses/by/3.0/"
local text2 = "\"Go Cart - Loop Mix\" Kevin MacLeod (incompetech.com) \nLicensed under Creative Commons: By Attribution 3.0 \nhttp://creativecommons.org/licenses/by/3.0/"
local text3 = "Options Icon made by Freepik from www.flaticon.com"
local text4 = "Back Icon made by Freepik from www.flaticon.com"

---------------------------------------------------------------------------------

--Close the credits. Who wants their name on the project?
function closeCredits( event )
  --Hide the overlay with the appropriate options
  if event.phase == "ended" then
    composer.hideOverlay("fade", 100)
  end
end

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view

   -- Initialize the scene here.
   -- Example: add display objects to "sceneGroup", add touch listeners, etc.

   --Create and set credits image. TODO: Insert pseudonyms. Improve GFX fidelity.
   credits = display.newImageRect("GFX/Common/whitebg.png", 440, 320)
   credits.x = display.contentCenterX
   credits.y = display.contentCenterY
   credits:setFillColor(0.5, 0.60, 0.5, 1)
   credits.strokeWidth = 4
   credits:setStrokeColor( 0, 0 ,0 )
   sceneGroup:insert(credits)

   --Text
   local options = {
     parent = sceneGroup,
     text = shame,
     x = display.contentCenterX,
     y = display.contentCenterY-105,
     font = native.systemFont,
     fontSize = 16,
     align = "center"
   }
   shame2 = display.newText(options)
   shame2:setTextColor(0,0,0)

   options = {
     parent = sceneGroup,
     text = "Main Developers\n---------------------------------------------------------------",
     x = display.contentCenterX,
     y = 25,
     font = native.systemFontBold,
     fontSize = 20,
     align = "center"
   }
   h1 = display.newText(options)
   h1:setTextColor(0,0,0)

   options = {
     parent = sceneGroup,
     text = "Music By\n---------------------------------------------------------------",
     x = display.contentCenterX,
     y = display.contentCenterY-70,
     font = native.systemFontBold,
     fontSize = 20,
     align = "center"
   }
   h2 = display.newText(options)
   h2:setTextColor(0,0,0)

   options = {
     parent = sceneGroup,
     text = text1,
     x = display.contentCenterX,
     y = display.contentCenterY-25,
     font = native.systemFont,
     fontSize = 15,
     align = "center"
   }
   music1 = display.newText(options)
   music1:setTextColor(0,0,0)

   options = {
     parent = sceneGroup,
     text = text2,
     x = display.contentCenterX,
     y = display.contentCenterY+30,
     font = native.systemFont,
     fontSize = 15,
     align = "center"
   }
   music2 = display.newText(options)
   music2:setTextColor(0,0,0)

   options = {
     parent = sceneGroup,
     text = "Icons:\n---------------------------------------------------------------",
     x = display.contentCenterX,
     y = display.contentCenterY+80,
     font = native.systemFontBold,
     fontSize = 20,
     align = "center"
   }
   h3 = display.newText(options)
   h3:setTextColor(0,0,0)

   options = {
     parent = sceneGroup,
     text = text3,
     x = display.contentCenterX,
     y = display.contentCenterY+110,
     font = native.systemFont,
     fontSize = 15,
     align = "center"
   }
   icon1 = display.newText(options)
   icon1:setTextColor(0,0,0)

   options = {
     parent = sceneGroup,
     text = text4,
     x = display.contentCenterX,
     y = display.contentCenterY+130,
     font = native.systemFont,
     fontSize = 15,
     align = "center"
   }
   icon2 = display.newText(options)
   icon2:setTextColor(0,0,0)
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase
   local parent = event.parent

   if ( phase == "will" ) then
      -- Called when the scene is still off screen (but is about to come on screen).
   elseif ( phase == "did" ) then
      -- Called when the scene is now on screen.
      -- Insert code here to make the scene come alive.
      -- Example: start timers, begin animation, play audio, etc.

      --Make it so the user can click the image to dismiss the credits.
      credits:addEventListener("touch", closeCredits)
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase
   local parent = event.parent

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
      --Remove the listener.
      credits:removeEventListener("touch", closeCredits)
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

   -- Called prior to the removal of scene's view ("sceneGroup").
   -- Insert code here to clean up the scene.
   -- Example: remove display objects, save state, etc.

   --Clean up the variable(s).
   credits:removeSelf()
   credits = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
