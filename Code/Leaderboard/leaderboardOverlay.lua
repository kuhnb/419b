local composer = require( "composer" )
local scene = composer.newScene()
local myData = require("Code.mydata")

---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here

local background
local level
local exit
local startScores =  display.contentHeight - 275
local scoreSpace = 20
local numberOfScores = 5
local scores = display.newGroup()
---------------------------------------------------------------------------------

local function addUser(rank, score)
  if(rank ~= nil and score ~= nil) then
    display.newText(scores, "...",
     display.contentCenterX, startScores + (numberOfScores + 1)*scoreSpace, native.systemFont, 24)
    display.newText(scores, myData.App42Username..score,
     display.contentCenterX, startScores + (numberOfScores + 2)*scoreSpace, native.systemFont, 24)
  end
end

local function createLeaderGroup(level)
  return function(array)
    --top of table
    display.newText(scores, "User           Scores",
     display.contentCenterX, startScores , native.systemFont, 12)
     local isTopScorer = false
     --scores
    for x=1,numberOfScores,1 do
      if(array[x]~= nil) then
        print(array)
        print("User: "..array[x][1])
        print("Score: "..array[x][2])
        display.newText(scores, "Rank "..x..": "..array[x][1].."  "..array[x][2],
        display.contentCenterX, startScores + (x)*scoreSpace, native.systemFont, 12)
        print(array[x][1].." == "..myData.App42Username.." = "..tostring(array[x][1] == myData.App42Username))
        if(array[x][1] == myData.App42Username) then
          isTopScorer = true
        end
      else
        numberOfScores = x
      end
    end
    if(isTopScorer == false) then
      getHighestScoresByUser(level, myData.App42Username , addUser)
    end
  end
end


--Close the options/pause menu,
local function closeLeader( event )
  --Hide the overlay with the appropriate options
  if event.phase == "ended" then
    composer.hideOverlay("fade", 250)
  end
end

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view

      --Generate and set background music
      background = display.newImageRect("GFX/Common/options.png", 320, 320)
      background.x = display.contentCenterX
      background.y = display.contentCenterY
      sceneGroup:insert(background)
      --Generate text and set colors
      level = display.newText(sceneGroup, "level "..event.params.level, display.contentCenterX, display.contentHeight - 300, native.systemFont, 24)
      level:setTextColor(0, 0, 0)

      exit = display.newText(sceneGroup, "Close", display.contentCenterX, display.contentHeight - 40, native.systemFont, 24)

      --local array = {}
      --array[1] = {}
      --array[2] = {}
      --array[3] = {}
      --array[4] = {}
      --array[5] = {}
      --array[1][1] = "cat"
      --array[2][1] = "asdfgasd"
      --array[3][1] = "fwerfwer"
      --array[4][1] = "sdfgserg"
      --array[5][1] = "dsfsd"
      --array[1][2] = 5
      --array[2][2] = 4
      --array[3][2] = 3
      --array[4][2] = 2
      --array[5][2] = 1
      --createLeaderGroup(array)
       getTopNRankers(event.params.level, numberOfScores, createLeaderGroup(event.params.level))
       sceneGroup:insert(scores)

   -- Initialize the scene here.
   -- Example: add display objects to "sceneGroup", add touch listeners, etc.
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is still off screen (but is about to come on screen).
   elseif ( phase == "did" ) then
      exit:addEventListener("touch", closeLeader)
      -- Called when the scene is now on screen.
      -- Insert code here to make the scene come alive.
      -- Example: start timers, begin animation, play audio, etc.
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is on screen (but is about to go off screen).
      -- Insert code here to "pause" the scene.
      -- Example: stop timers, stop animation, stop audio, etc.
   elseif ( phase == "did" ) then
   exit:removeEventListener("touch", closeLeader)
      -- Called immediately after scene goes off screen.
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

   exit:removeSelf()
   exit = nil
   -- Called prior to the removal of scene's view ("sceneGroup").
   -- Insert code here to clean up the scene.
   -- Example: remove display objects, save state, etc.
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
