local composer = require( "composer" )
local scene = composer.newScene()
local widget = require( "widget" )

---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here

local background
local backButton
local scrollBox
local page = 0
local forwardButton
local previousButton
local outOfLevels = false
local buttons = {}
local gameNames = {}
local backButton
---------------------------------------------------------------------------------
--Return to select game screen. So the player can play the good game.
local function toSelect()
--Generate options.
local options = {
effect = "fade",
time = 1000
}
audio.play(soundEffect, {channel=2})
composer.gotoScene("Code.gameSelect", options)
end

--Open options overlay
local function openLeader(level)

  --Generate options
  local options = {
    effect = "fade",
    --This ensures players cannot interact with menu while options are being displayed.
    isModal = true,
    time = 500,
    params = {
      level = level
    }
  }
  return function(event)
      composer.showOverlay("Code.Leaderboard.leaderboardOverlay", options)
  end
end

local function initializeValues(fxn)
  return function(result)
      gameNames = result
      fxn()
    end
end

local function getValues()
  local values = {}
  outOfLevels = false
  values[1] = gameNames[1 + page*5]
  values[2] = gameNames[2 + page*5]
  values[3] = gameNames[3 + page*5]
  values[4] = gameNames[4 + page*5]
  values[5] = gameNames[5 + page*5]
  if gameNames[6 + page*5] == nil then
    outOfLevels = true
  end
  return values
end

function createLeaderButtons()
  local values = getValues()
  local sceneGroup = scene.view

    for i=1,5 do
      if(buttons ~= nil) then
        sceneGroup:remove(buttons[i])
        buttons[i] = nil
      end
      if(values[i] ~= nil) then
          local s = tostring(values[i])
          local abc = s:sub(1, 1)
          local def = s:sub(2, 2)
          local l = ""
          if abc ~= "G" then
            l = "Level ".. abc .. "-" .. def
          else
            l = values[i]
          end
        buttons[i] = widget.newButton{
          label = l,
          left = display.contentCenterX - 50,
          id = "Button "..i,
          top = (12 + i*50),
         onRelease = openLeader(values[i]),
          --properties for a rounded rectangle button...
         shape="roundedRect",
         width = 100,
         height = 40,
         cornerRadius = 2,
         font = native.systemFont,
         fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
         labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
         strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
         strokeWidth = 4,
       }
       sceneGroup:insert(buttons[i])
         --outOfLevels = false
       else
         outOfLevels = true
       end

    end
end

local createPageButtons

local function createPage()
  createLeaderButtons()
  createPageButtons()
end
local function backOnePage()
  page = page - 1
  createPage()
end

local function forwardOnePage()
    page = page + 1
    createPage()
end

createPageButtons = function()

  local sceneGroup = scene.view
  if(previousButton ~= nil) then
    sceneGroup:remove(previousButton)
    sceneGroup:remove(forwardButton)
    previousButton = nil
    forwardButton = nil
  end
 if page > 0 then
   previousButton = widget.newButton{
     label = "Previous",
     left = 5,
     id = "Previous",
     top = display.contentCenterY - 25,
     onRelease = backOnePage,
     --properties for a rounded rectangle button...
    shape="roundedRect",
    width = 75,
    height = 50,
    cornerRadius = 2,
    fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
    labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
    strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
    strokeWidth = 4,
   }
 else
     previousButton = widget.newButton{
       label = "Previous",
       left = 5,
       id = "Previous",
       top = display.contentCenterY - 25,
       --properties for a rounded rectangle button...
      shape="roundedRect",
      width = 75,
      height = 50,
      cornerRadius = 2,
      fillColor = { default={0.5}, over={ 0.38, 0.27, 0.32, 1 } },
      labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
      strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
      strokeWidth = 4,
     }
 end

 if outOfLevels == false then
   forwardButton = widget.newButton{
     label = "Forward",
     left = display.contentWidth - 84,
     id = "Forward",
     top = display.contentCenterY - 25,
     onRelease = forwardOnePage,
     --properties for a rounded rectangle button...
    shape="roundedRect",
    width = 75,
    height = 50,
    cornerRadius = 2,
    fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
    labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
    strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
    strokeWidth = 4,
   }
 else
     forwardButton = widget.newButton{
       label = "Forward",
       left = display.contentWidth - 84,
       id = "Forward",
       top = display.contentCenterY - 25,
       --properties for a rounded rectangle button...
      shape="roundedRect",
      width = 75,
      height = 50,
      cornerRadius = 2,
      fillColor = { default={ 0.5}, over={0.5} },
      labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
      strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
      strokeWidth = 4,
     }
 end
 backButton = widget.newButton{
         label = "To Menu",
         left = display.contentWidth - 95,
         id = "Main",
         top = display.contentHeight - 45,
        onRelease = toSelect,
         --properties for a rounded rectangle button...
        shape="roundedRect",
        width = 90,
        height = 40,
        cornerRadius = 2,
        fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
        labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
        strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
        strokeWidth = 4,
     }
 sceneGroup:insert(forwardButton)
 sceneGroup:insert(previousButton)
 sceneGroup:insert(backButton)
end

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   background = display.newImageRect("GFX/Common/title.png", 480, 320)
   background.anchorX = 0
   background.anchorY = 0
   background.x = 0
   background.y = 0
   sceneGroup:insert(background)
   title = display.newText(sceneGroup, "Leaderboards", display.contentCenterX, 40, native.systemFont, 42)
   getGamesList(initializeValues(createPage))
   -- Initialize the scene here.
   -- Example: add display objects to "sceneGroup", add touch listeners, etc.
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is still off screen (but is about to come on screen).
   elseif ( phase == "did" ) then
      -- Called when the scene is now on screen.
      -- Insert code here to make the scene come alive.
      -- Example: start timers, begin animation, play audio, etc.
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is on screen (but is about to go off screen).
      -- Insert code here to "pause" the scene.
      -- Example: stop timers, stop animation, stop audio, etc.
   elseif ( phase == "did" ) then
      -- Called immediately after scene goes off screen.
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

   -- Called prior to the removal of scene's view ("sceneGroup").
   -- Insert code here to clean up the scene.
   -- Example: remove display objects, save state, etc.
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
