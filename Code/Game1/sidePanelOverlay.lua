local composer = require( "composer" )
local scene = composer.newScene()
local myData = require("Code.mydata")

---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here
---------------------------------------------------------------------------------
-- Drag and drop variables

local numberOfOR = myData.OR
local numberOfAND = myData.AND
local numberOfTRUE = myData.TRUE
local numberOfFALSE = myData.FALSE

local sideTabImage = "GFX/Common/title.png"
local ORImage = myData.graphicsSelected.."or.png"
local ANDImage = myData.graphicsSelected.."and.png"
local TRUEImage = myData.graphicsSelected.."true.png"
local FALSEImage = myData.graphicsSelected.."false.png"

local ORButton
local ANDButton
local TRUEButton
local FALSEButton

local ORRemaining
local ANDRemaining
local TRUERemaining
local FALSERemaining

local buttonPressed

local back

local sideTab
local sideTabButton

local panelWidth = 100

---------------------------------------------------------------------------------
--Drag and drop button section

function addToNumberOfButtonsLeft(value, number)
  if(value == "OR") then
    myData.numberOfOR = myData.numberOfOR + number
  elseif(value == "AND") then
    myData.numberOfAND = myData.numberOfAND + number
  elseif(value == "TRUE") then
    myData.numberOfTRUE = myData.numberOfTRUE + number
  elseif(value == "FALSE") then
    myData.numberOfFALSE = myData.numberOfFALSE + number
  else
    print("ATTEMPTED TO ADD NUMBER TO INVALID BUTTON VALUE "..value)
  end
end

function closeTab(event)
  composer.hideOverlay( "slideRight", 100 )
end

function createDragableButton(event)
  if event.phase == "began" then
    if event.target == ORButton then
      buttonPressed = "OR"
      addToNumberOfButtonsLeft("OR", -1)
    elseif event.target == ANDButton then
      buttonPressed = "AND"
      addToNumberOfButtonsLeft("AND", -1)
    elseif event.target == TRUEButton then
      buttonPressed = "TRUE"
      addToNumberOfButtonsLeft("TRUE", -1)
    elseif event.target == FALSEButton then
      buttonPressed = "FALSE"
      addToNumberOfButtonsLeft("FALSE", -1)
    end
    closeTab()
  end
end

function initializePanel(event, sceneGroup)
  local panelGroup = display.newGroup()
  panelGroup.x = display.contentWidth - panelWidth/2
  panelGroup.y = display.contentHeight/2

  sideTab = display.newImageRect(sideTabImage, panelWidth, display.contentHeight)

  ORRemaining = display.newText( myData.numberOfOR, 0, -67, native.systemFont, 14 )
  ORButton = display.newImageRect(ORImage,42,42)
  ORButton.x = 0
  ORButton.y = -100
  if(myData.numberOfOR == nil) then
    myData.numberOfOR = 0
  end
  if(myData.numberOfOR > 0) then
    ORButton:addEventListener("touch",createDragableButton)
  else
    ORButton:setFillColor(0.3,0.3,0.3)
  end

  ANDRemaining = display.newText( myData.numberOfAND, 0, -2, native.systemFont, 14 )
  ANDButton = display.newImageRect(ANDImage,42,42)
  ANDButton.x = 0
  ANDButton.y = -35
  if(myData.numberOfAND == nil) then
    myData.numberOfAND = 0
  end
  if(myData.numberOfAND > 0) then
    ANDButton:addEventListener("touch",createDragableButton)
  else
    ANDButton:setFillColor(0.3,0.3,0.3)
  end

  TRUERemaining = display.newText( myData.numberOfTRUE, 0, 68, native.systemFont, 14 )
  TRUEButton = display.newImageRect(TRUEImage,60,42)
  TRUEButton.x = 0
  TRUEButton.y = 35
  if(myData.numberOfTRUE == nil) then
    myData.numberOfTRUE = 0
  end
  if(myData.numberOfTRUE > 0) then
    TRUEButton:addEventListener("touch",createDragableButton)
  else
    TRUEButton:setFillColor(0.3,0.3,0.3)
  end

  FALSERemaining = display.newText( myData.numberOfFALSE, 0, 133, native.systemFont, 14 )
  FALSEButton = display.newImageRect(FALSEImage,60,42)
  FALSEButton.x = 0
  FALSEButton.y = 100
  if(myData.numberOfFALSE == nil) then
    myData.numberOfFALSE = 0
  end
  if(myData.numberOfFALSE > 0) then
    FALSEButton:addEventListener("touch",createDragableButton)
  else
    FALSEButton:setFillColor(0.3,0.3,0.3)
  end

  panelGroup:insert(sideTab)
  panelGroup:insert(ORButton)
  panelGroup:insert(ORRemaining)
  panelGroup:insert(ANDButton)
  panelGroup:insert(ANDRemaining)
  panelGroup:insert(TRUEButton)
  panelGroup:insert(TRUERemaining)
  panelGroup:insert(FALSEButton)
  panelGroup:insert(FALSERemaining)

  back = display.newRect( display.contentWidth/2 - panelWidth/2, display.contentHeight/2,
                                    display.contentWidth - panelWidth, display.contentHeight )
  back:setFillColor(0)
  back.alpha = 0.01
  back:addEventListener("touch", closeTab)

  sceneGroup:insert(back)
  sceneGroup:insert(panelGroup)
end
---------------------------------------------------------------------------------

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   -- Initialize the scene here.
   -- Example: add display objects to "sceneGroup", add touch listeners, etc.
   initializePanel(event, sceneGroup)
   buttonPressed = nil
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is still off screen (but is about to come on screen).
   elseif ( phase == "did" ) then
      -- Called when the scene is now on screen.
      -- Insert code here to make the scene come alive.
      -- Example: start timers, begin animation, play audio, etc.
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase
   local parent = event.parent

   if ( phase == "will" ) then
      -- Called when the scene is on screen (but is about to go off screen).
      -- Insert code here to "pause" the scene.
      -- Example: stop timers, stop animation, stop audio, etc.
   elseif ( phase == "did" ) then
      -- Called immediately after scene goes off screen.
      ORButton:removeEventListener("touch", createDragableButton)
      ANDButton:removeEventListener("touch", createDragableButton)
      TRUEButton:removeEventListener("touch", createDragableButton)
      FALSEButton:removeEventListener("touch", createDragableButton)
      back:removeEventListener("touch", closeTab)
      if(buttonPressed ~= nil)then
        parent:createDragable(buttonPressed)
      end
   end
end

-- "scene:destroy()"
function scene:destroy( event )
  ORButton:removeSelf()
  ANDButton:removeSelf()
  TRUEButton:removeSelf()
  FALSEButton:removeSelf()
  back:removeSelf()
  ORButton = nil
  ANDButton = nil
  TRUEButton = nil
  FALSEButton = nil
  back = nil
 local sceneGroup = self.view

   -- Called prior to the removal of scene's view ("sceneGroup").
   -- Insert code here to clean up the scene.
   -- Example: remove display objects, save state, etc.
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
