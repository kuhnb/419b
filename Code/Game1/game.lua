local composer = require( "composer" )
local scene = composer.newScene()
local myData = require("Code.mydata")
local parse = require( "mod_parse" )
local widget = require("widget")
local curr = require("Code.currency")

---------------------------------------------------------------------------------

-- local forward references should go here

local gamebg
local exit
local nextNum
local checkButton
local backButton
local pauseButton
local switchVal
local numboop
local numbool
local opti
local optionsButton
---------------------------------------------------------------------------------
-- Drag and drop variables
--an array that stores all the drop locations (not used, see locations instead)
local DropLocationGroup = {}
--the image used for the drop location
local DropLocationImage = "GFX/Common/title.png"
--how many drop locations are there (not used, see locationsize instead)
local numberOfDropLocation = 0
--image used for the side panel
local sideTabButtonImage = "GFX/Common/back.png"
--the side panel itself
local sideTabButton

--variables to hold how many of each button is given per level
local ORLevel = {2,0,0}
local ANDLevel = {0,1,0}
local TRUELevel = {0,0,2}
local FALSELevel = {0,0,2}

---------------------------------------------------------------------------------
--Spawn dropLocation
--the class that deals with drop location that the elements spawn on
local SpawnDrop = {}
SpawnDrop.__index = SpawnDrop
--the object that is the spawn location
local spawn

--constructor function
function SpawnDrop.new()
  local self = setmetatable({}, SpawnDrop)
  self.__index = self
  self.image = DropLocationImage
  self.button = display.newImageRect(self.image, 42,42)
  return self
end

--get the button for this object
function SpawnDrop.getButton(self)
  return self.button
end
--remove the Spawn item from the scene
function removeSpawnFromScene(sp)
  sp:getButton():removeSelf()
  sp = nil
end
---------------------------------------------------------------------------------
--Drop Locations
--locations of the drop locations
local locations = {}
--how many drop locations are there
local locationsSize = 0
--class for a drop location
local  DropLocation = {}
--how far away can a dragable button be before it snaps to a location
local maximumSnapDistance = 35
--an array that holds all the dragable buttons
local dragables = {}
--how many dragable buttons there are
local dragablesSize = 0
--add class to the table
DropLocation.__index = DropLocation

--are these two locations within a distance of maximumSnapDistance?
function closeEnough(x1,y1,x2,y2)
  return (maximumSnapDistance >= math.sqrt(math.pow(x1 - x2 , 2) + math.pow(y1 - y2 , 2)))
end

--snaps a button to a dropLocation or to the spawn which will delete the button
--This function will look at the location of the given button and try and find a location
--that is suitible for it. Returns true if a button is snapped
function snapToLocation(btn)
  --if the button should snap to the spawn
  if(closeEnough(btn.x,btn.y,spawn:getButton().x,spawn:getButton().y)) then
    removeDragableFromScene(dragables[btn.index])
  end
  local locBtn
  for i, Value in pairs( locations ) do
    locBtn = locations[i]:getButton()
    if(closeEnough(btn.x,btn.y,locBtn.x,locBtn.y))then
      if(locations[i]:getValue() ~= nil)then
        btn.x = btn.startX
        btn.y = btn.startY
        return false
      end
      switchVal = switchVal + 1
      btn.x = locBtn.x
      btn.y = locBtn.y
      locations[i]:setValue(dragables[btn.index]:getValue())
      btn.loc = i
      return true
    end

  end
  return false
end

--constructor for a dropLocation
function DropLocation.new(init,image)
  local self = setmetatable({}, DropLocation)
  self.__index = self
  self.value = init
  self.image = image
  self.button = display.newImageRect(self.image, 42,42)
  locations[locationsSize] = self
  locationsSize = locationsSize + 1
  return self
end

--adds number to the number of value buttons left
function addToNumberOfButtonsLeft(value, number)
  if(value == "OR") then
    myData.numberOfOR = myData.numberOfOR + number
  elseif(value == "AND") then
    myData.numberOfAND = myData.numberOfAND + number
  elseif(value == "TRUE") then
    myData.numberOfTRUE = myData.numberOfTRUE + number
  elseif(value == "FALSE") then
    myData.numberOfFALSE = myData.numberOfFALSE + number
  else
    print("ATTEMPTED TO ADD NUMBER TO INVALID BUTTON VALUE "..value)
  end
end

-- get the value of the button in the drop location
function DropLocation.getValue(self)
  return self.value
end

--set the value in the drop location
function DropLocation.setValue(self, newval)
  self.value = newval
end

--get the button that is the drop location
function DropLocation.getButton(self)
  return self.button
end
---------------------------------------------------------------------------------
--Drag and drop Buttons
local dragableImages = {["OR"] = myData.graphicsSelected.."or.png",
                        ["AND"] = myData.graphicsSelected.."and.png",
                        ["TRUE"] = myData.graphicsSelected.."true.png",
                        ["FALSE"] = myData.graphicsSelected.."false.png",}

local Dragable = {} -- the table representing the class, which will double as the metatable for the instances
Dragable.__index = Dragable -- failed table lookups on the instances should fallback to the class table, to get methods

--remove the draggable item from the scene
function removeDragableFromScene(item)
  addToNumberOfButtonsLeft(item:getValue(), 1)
  item:getButton():removeSelf()
  item:getButton():removeEventListener("touch",dragableListener)
  item = nil
end

--the listener for the dragable object
--this is where the drag and drop functionality takes place
function dragableListener(event)
      if event.phase == "began" then
            if(event.target.first == nil) then
              event.target.first = true
              sideTabButton:addEventListener("touch", openSideTab)
            end
            if(event.target.loc ~= nil) then
              locations[event.target.loc ]:setValue(nil)
              event.target.loc = nil
            end
          event.target.startX = event.target.x    -- store x location of object
          event.target.startY = event.target.y    -- store y location of object

      elseif event.phase == "moved" then
        --possible fix to the startX nil bug.
        --if the drag even is carried between scenes ignore it
          if(event.target.startX == nil or event.target.startY == nil)then
            return
          end
          local x = (event.x - event.xStart) + event.target.startX
          local y = (event.y - event.yStart) + event.target.startY

          event.target.x, event.target.y = x, y    -- move object based on calculations above
      elseif event.phase == "ended"  or event.phase == "cancelled" then
        snapToLocation(event.target)
      end

      return true
end

--constructor for dragable
function Dragable.new(init)
  local self = setmetatable({}, Dragable)
  self.value = init
  self.image = dragableImages[self.value]
  local width = 42
  if (self.value == "TRUE" or self.value == "FALSE") then
    width = 60
  end
  self.button = display.newImageRect(self.image, width , 42)
  self.button.x = display.contentWidth - 21
  self.button.y = display.contentHeight - 21
  dragables[dragablesSize] = self
  self.index = dragablesSize
  self.button.index = dragablesSize
  dragablesSize = dragablesSize + 1
  return self
end

--set the value of the dragable
function Dragable.setValue(self, newval)
  self.value = newval
end

--get the value of the dragable
function Dragable.getValue(self)
  return self.value
end

--get the button that is the dragable
function Dragable.getButton(self)
  return self.button
end


---------------------------------------------------------------------------------
--toggle button variables
local logicButtonGroup = {}
local logicImages = {[0] ="GFX/Game1/and.png",[1] ="GFX/Game1/or.png"}
local logicButtonValues = {}
local numberOfLogicButtons = 0
local maxLogicValue = 1

local trueFalseButtonGroup = {}
local trueFalseImages = {[0] ="GFX/Game1/false.png",[1] ="GFX/Game1/true.png"}
local trueFalseButtonValues = {}
local numberOfTrueFalseButtons = 0
local maxtrueFalseValue = 1

local startTime
local endTime
local errorCount

--For the score of each level, the value will be stored in levelScore, and calculated in function "calculateLevelScore()".
local levelScore
--The leveldifficulty should be: 0=Easy, 1=Medium, 2=Hard
local levelDifficulty


---------------------------------------------------------------------------------

local bgmusic = audio.loadStream("SFX/Music/Daily Beetle.mp3")

---------------------------------------------------------------------------------

--Used to calculate the final "levelScore" value.
function calculateLevelScore()
 --levelScore = (Level Max Score)*(Difficulty Value) - (Seconds to Complete).
  levelScore = -1
 --HARDCODED:
  levelDifficulty = 0
 --Max Level Scores:
  local maxLevel1=100
  local maxLevel2=120
  local maxLevel3=300
  local maxLevel4=120
  local maxLevel5=120
 --Difficulty Modifiers:
  local easy=1
  local medium=1.5
  local hard=2
  --Name of Game
  local nameOfGame

  --If a valid level number
  if nextNum > 0 and nextNum < 6 then
    --Step 1) Set levelDifficulty
    if levelDifficulty == 0 then
      levelDifficulty = easy
     elseif levelDifficulty == 1 then
      levelDifficulty = medium
     elseif levelDifficulty == 2 then
      levelDifficulty = hard
     else
      print("Error Calculating Level Difficulty. (In: function calculateLevelScore())")
      print("levelDifficulty: " .. levelDifficulty)
    end
    --Step 2) Set levelScore = the current level * the difficulty modifier
    if nextNum == 2 then
      nameOfGame="11"
      levelScore = maxLevel1 * levelDifficulty
    elseif nextNum == 3 then
      nameOfGame="12"
      levelScore = maxLevel2 * levelDifficulty
    elseif nextNum == 4 then
      nameOfGame="13"
      levelScore = maxLevel3 * levelDifficulty
    end
    --Step 3) Decrement levelScore by endTime
    if levelScore > 0 then
      levelScore = levelScore - endTime
      if levelScore < 0 then
        levelScore = 0
      end
    end
    --Step 4) Math.Floor value
    levelScore = math.floor(levelScore)
    --TEST: Print Level Score
    print("LEVEL "..nextNum.." SCORE: "..levelScore)

    --Step 5) Currency. Increment Player's Coins
    changeUserCoins(levelScore)

    --TODO: APP42 Stuff

    local usr = myData.App42Username
    --Step 5) Save Score on App42
    local globalCall = function(adding)
      print("global created "..adding)
      return function(score)
          if score == nil then
            score = 0
          end
          print("global called "..score)
          saveUserScore("global",usr,  adding + score)
          print("Hello"..score)
        end
    end

    local call = function(score)
      if score == nil then
        score = 0
      end
      if(score < levelScore) then
        saveUserScore(nameOfGame,usr,levelScore)
        getHighestScoresByUser("Global",usr,globalCall(levelScore - score))
      end
    end

    getHighestScoresByUser(nameOfGame,usr,call)

  else
    --0 > nextNum >= 6. Not currently supported. Show on console.
    print("Error Calculating Level Score. (In: function calculateLevelScore())")
    print("Level Number: " .. nextNum)
  end



end --end function calculateLevelScore()


--Transition to the title screen.
local function toTitle(event)
  --Generate appropriate options
  local options = {
    effect = "fade",
    time = 1000
  }
  --Go to title.lua
  if event.phase == "ended" then
    audio.stop(1)
    composer.gotoScene("Code.Game1.title", options)
  end
end

function scene:optionsToTitle(event)
  --Generate appropriate options
  local options = {
    effect = "fade",
    time = 1000
  }
  --Go to title.lua
  if event.phase == "ended" then
    audio.stop(1)

    composer.gotoScene("Code.Game1.title", options)
    composer.hideOverlay("fade", 250)
  end
end

--To ensure everything is reinitialized properly, we go to a new scene and transition back here.
local function nextLevel(event)
  local num = nextNum
  local attribute = "G1L"..(nextNum-1)
  --Generate appropriate options and parameters
  local options = {
    effect = "fade",
    time = 500,
    params = {
      num = nextNum,
      level = "1-" .. num .. ".png"
    }
  }
  --Go to nextLevel.lua
  if event.phase == "ended" then
    --print("SwitchVal = " .. switchVal)
    --C-c-combo. User has played multiple levels in a row. Increment counter.
    if myData.g1Combo then
      myData.g1Combo = myData.g1Combo + 1
    else
      myData.g1Combo = 1
    end
    --ComboCorrect - the number of levels correctly answered in a row
    if myData.g1ComboCorrect then
      if errorCount == 0 then
        myData.g1ComboCorrect = myData.g1ComboCorrect + 1
      else
        if myData.g1ComboCorrect > myData.g1RecordComboCorrect then
          myData.g1RecordComboCorrect = myData.g1ComboCorrect
        end
        myData.g1ComboCorrect = 0
      end
    else
      if errorCount == 0 then
        myData.g1ComboCorrect = 1
      else
        myData.g1ComboCorrect = 0
      end
    end
    --Send per level metrics to parse
    if myData.consent == true then
      parse:updateObject("LevelTime", myData.ltid, {[attribute] = endTime})
      parse:updateObject("NumMistake", myData.nmid, {[attribute] = errorCount})
      parse:updateObject("LevelPlay", myData.lpid, {[attribute] = myData.np[attribute]})
      parse:updateObject("NumBoopl", myData.nbplid, {[attribute .. "Op"] = numboop, [attribute .. "Boolean"] = numbool})
      parse:updateObject("SwitchVal", myData.svid, {[attribute] = switchVal})
    end
    --Transition scene
    composer.gotoScene("Code.Game1.nextLevel", options)
  end
end

--Check if answer is correct.
--TODO: Create general case checker.
local function checkAnswer(event)
  local locBtn
  if (event.phase == "ended") then
    if (nextNum == 2) then
      if (locations[0]:getValue() ~= nill and locations[1]:getValue() ~= nil) then
        --Calculate LevelTime metric
        endTime = system.getTimer()/1000 - startTime
        calculateLevelScore()
        --Count the number of times a level was played.
        --Nifty trick with lua: nil values return as false!
        if myData.np["G1L1"]then
          myData.np["G1L1"] = myData.np["G1L1"] + 1
        else
          myData.np["G1L1"] = 1
        end
        --We done here yo.
        nextLevel(event)
      end
      --Incorrect operator metrics collection goes here
      if locations[0]:getValue() ~= "OR" then numboop = numboop + 1 end
      if locations[1]:getValue() ~= "OR" then numboop = numboop + 1 end
    elseif (nextNum == 3) then
      if (locations[0]:getValue() == "AND") then
        --Calculate end time metric
        endTime = system.getTimer()/1000 - startTime
        calculateLevelScore()
        --Count the number of times level 2 was palyed
        if myData.np["G1L2"]then
          myData.np["G1L2"] = myData.np["G1L2"] + 1
        else
          myData.np["G1L2"] = 1
        end
        --Just move along.
        nextLevel(event)
      end
      --Incorrect operator metrics collection goes here
      if locations[0]:getValue() ~= "AND" then numboop = numboop +1 end
    elseif (nextNum == 4) then
      if( not locations[0]:getValue() or locations[1]:getValue()) then
        if(locations[2]:getValue() == "FALSE" and locations[3]:getValue() == "TRUE") then
          --Calculate LevelTime metric
          endTime = system.getTimer()/1000 - startTime
          calculateLevelScore()
          --Calculate the number of times level 3 was played
          if myData.np["G1L3"] then
            myData.np["G1L3"] = myData.np["G1L3"] + 1
          else
            myData.np["G1L3"] = 1
          end
          --We're still going hey?
          nextLevel(event)
        end
        --Incorrect operator metrics collection goes here
        if locations[0]:getValue() ~= "FALSE" then numbool = numbool + 1 end
        if locations[1]:getValue() ~= "TRUE" then numbool = numbool + 1 end
        if locations[2]:getValue() ~= "FALSE" then numbool = numbool + 1 end
        if locations[3]:getValue() ~= "TRUE" then numbool = numbool + 1 end
      end
    else
      print("Error in selecting level. (game.lua)")
    end
    --Someone made a booboo. Increment NumMistake metric
    errorCount = errorCount+1
  end
end

function resetNumberOfButtons()
  myData.numberOfOR = 0
  myData.numberOfAND = 0
  myData.numberOfTRUE = 0
  myData.numberOfFALSE = 0
end

--Clean uop all elements in a given group
local function removeGroupFromScene(event, group)
  for i, Value in pairs( group ) do
    if(group[i] ~= nil) then
      group[i]:removeSelf()
      group[i] = nil
    end
  end
end

--Remove all listeners in a given group
local function removeListenerFromGroup(event,group,listener)
  for i, Value in pairs( group ) do
    if(group[i] ~= nil) then
      group[i]:removeEventListener("touch", listener)
    end
  end
end

--Clean up all elements in a given group
function removeClassGroupFromScene(event, group)
  for i, Value in pairs( group ) do
    if(group[i] ~= nil) then
      group[i]:getButton():removeSelf()
      group[i] = nil
    end
  end
end

--Remove all listeners in a given group
function removeClassListenerFromGroup(event,group,listener)
  for i, Value in pairs( group ) do
    if(group[i] ~= nil) then
      group[i]:getButton():removeEventListener("touch", listener)
    end
  end
end

--Open options overlay
local function openOptions(event)
  --Generate options
  local options = {
    effect = "fade",
    --This ensures players cannot interact with menu while options are being displayed.
    isModal = true,
    time = 500,
    params = {
      text1 = "Resume"
    }
  }
  --Show the options overlay.
  if event.phase == "ended" then
    composer.showOverlay("Code.options", options)
  end
end

---------------------------------------------------------------------------------

--Toggle Buttons section TODO Make drag and drop elements.

--this function will find an element 'item' in the array 'arr' and return the index.
--if it cannot find the item it will return -1
function findIndex(arr, item)
  for i, Value in pairs( arr ) do
    if(arr[i] == item) then
      return i
    end
  end
  return -1
end

--this function will change the image of a toggle button
--NOTE: as the toggle button will be destroyed in this function
--it must be replaced into the group it was in and given a new listener
--image - the button you wish to change
--newPath - path to the image you wish to change it to
--number - the index in the group array at which the button is placed
--group - the group array that stores the toggle button
--listener = the listener for the toggle button
function changeImage(image, newPath, number, group, listener)
  w = image.width
  h = image.height
  x = image.x
  y = image.y
  image:removeEventListener("touch", listener)
  image:removeSelf()
  image = nil
  image = display.newImageRect(newPath, w, h)
  image.x = x
  image.y = y
  image:addEventListener("touch", listener)

  group[number] = image
end

--this function is the event that will take plave when the logic buttons are touched
function logicButtonPress(event)
  if event.phase == "began" then
    btn = event.target
    number = findIndex(logicButtonGroup, btn)
    if(number == -1) then
      print("ERROR: BUTTON NOT FOUND")
      return
    end
    if (logicButtonValues[number] >= maxLogicValue) then
      logicButtonValues[number] = 0
    else
      logicButtonValues[number] = logicButtonValues[number] + 1
    end
    changeImage(btn,logicImages[logicButtonValues[number]],number,logicButtonGroup,logicButtonPress)
  end
end

--this is the event that will take place when the true/false buttons are pressed
--NOTE: see logic button press
function trueFalseButtonPress(event)
  if event.phase == "ended" then
    btn = event.target
    number = findIndex(trueFalseButtonGroup, btn)
    if(number == -1) then
      print("ERROR: BUTTON NOT FOUND")
      return
    end
    if (trueFalseButtonValues[number] >= maxtrueFalseValue) then
      trueFalseButtonValues[number] = 0
    else
      trueFalseButtonValues[number] = trueFalseButtonValues[number] + 1
    end
    changeImage(btn,trueFalseImages[trueFalseButtonValues[number]],number,trueFalseButtonGroup,trueFalseButtonPress)
  end
end

--this function will add a new toggle button to it's group
--x - the x position of the button
--y - the y position of the button
--w - the width of the button
--h - the height of the button
--values - the array that contains the values of the buttons group
--group - the group which the button will belong to
--images - the group of images this button will use
--listener - the listener which the button will use
function addToggleButton(event, x, y, w, h, values, group, images, listener)
  number = 0
  for i, Value in pairs( group ) do
    number = number + 1
  end
  print(number)
  if(number == nil) then
    number = 0
  end
  values[number] = 0
  local btn = display.newImageRect(images[0], w, h)
  btn.x = x
  btn.y = y
  btn:addEventListener("touch", listener)
  group[number] = btn
end

function addGroupToScene(event, group, sceneGroup)
  --local sceneGroup = self.view
  if group == nil then
    return
  end

  for i, Value in pairs( group ) do
    if group[i] ~= nil then
      sceneGroup:insert(group[i])
    end
  end
end

function addClassGroupToScene(event, group, sceneGroup)
  --local sceneGroup = self.view
  if group == nil then
    return
  end

  for i, Value in pairs( group ) do
    if group[i] ~= nil then
      sceneGroup:insert(group[i]:getButton())
    end
  end
end
---------------------------------------------------------------------------------
function openSideTab(event)
  local options = {
      effect = "fromRight",
      isModal = true,
      time = 100
  }

  composer.showOverlay( "Code.Game1.sidePanelOverlay", options )
end

function initializeSidePanel(sceneGroup)
  sideTabButton = display.newImageRect(sideTabButtonImage, 20, 50)
  sideTabButton.x = display.contentWidth - 10
  sideTabButton.y = display.contentHeight/2
  sideTabButton:addEventListener("touch", openSideTab)
  sceneGroup:insert(sideTabButton)
end

function scene:createDragable(button)
  print(button)
  if(button == nil)then
    return
  end
  local sceneGroup = self.view
  local created = Dragable.new(button)
  created:getButton():addEventListener("touch", dragableListener)
  sideTabButton:removeEventListener("touch", openSideTab)
  sceneGroup:insert(created:getButton())
end
---------------------------------------------------------------------------------
function createSpawnLocation()
  if (spawn ==nil) then
    spawn = SpawnDrop.new()
    spawn:getButton().x = display.contentWidth - 21
    spawn:getButton().y = display.contentHeight - 21
  end
end

function level1Build()
  myData.numberOfOR = 2
  --add logic button
  --addToggleButton(event, 145, 125, 42,42,logicButtonValues,logicButtonGroup,logicImages,logicButtonPress)
    --add logic button
    --addToggleButton(event, 290, 220, 42,42,logicButtonValues,logicButtonGroup,logicImages,logicButtonPress)
    local a = DropLocation.new(nil, DropLocationImage)
    a:getButton().x = 145
    a:getButton().y = 135
    local b = DropLocation.new(nil, DropLocationImage)
    b:getButton().x = 290
    b:getButton().y = 220


end

function level2Build()
  myData.numberOfOR = 1
  myData.numberOfAND = 1
  --add logic button
  --addToggleButton(event, 250, 110, 42,42,logicButtonValues,logicButtonGroup,logicImages,logicButtonPress)
  local a = DropLocation.new(nil, DropLocationImage)
  a:getButton().x = 250
  a:getButton().y = 110
end

function level3Build()
  myData.numberOfTRUE = 2
  myData.numberOfFALSE = 2
  --addToggleButton(event, 58, 30, 60,42,trueFalseButtonValues,trueFalseButtonGroup,trueFalseImages,trueFalseButtonPress)
  --addToggleButton(event, 152, 30, 60,42,trueFalseButtonValues,trueFalseButtonGroup,trueFalseImages,trueFalseButtonPress)
  --addToggleButton(event, 325, 30, 60,42,trueFalseButtonValues,trueFalseButtonGroup,trueFalseImages,trueFalseButtonPress)
  --addToggleButton(event, 423, 30, 60,42,trueFalseButtonValues,trueFalseButtonGroup,trueFalseImages,trueFalseButtonPress)
  local a = DropLocation.new(nil, DropLocationImage)
  a:getButton().x = 58
  a:getButton().y = 30
  local b = DropLocation.new(nil, DropLocationImage)
  b:getButton().x = 152
  b:getButton().y = 30
  local c = DropLocation.new(nil, DropLocationImage)
  c:getButton().x = 325
  c:getButton().y = 30
  local d = DropLocation.new(nil , DropLocationImage)
  d:getButton().x = 423
  d:getButton().y = 30
end

---------------------------------------------------------------------------------
-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   sceneView = self.view
   switchVal = 0
   numboop = 0
   numbool = 0
   if not myData.np then
     myData.np = {}
   end
   --Inrement level number. TODO: Optimize this so we don't need multiple
   nextNum = event.params.num + 1
   errorCount = 0
   startTime = system.getTimer()/1000
   --Create and set background image. TODO: Make general case level builder.
   gamebg = display.newImageRect("Levels/Game1/" .. event.params.level, 480, 320)
   gamebg.anchorX = 0
   gamebg.anchorY = 0
   gamebg.x = 0
   gamebg.y = 0
   sceneGroup:insert(gamebg)

   --Create and set an options button
   optionsButton = widget.newButton{
     left = 0,
     top = display.contentHeight-43,
     shape = "roundedRect",
     font = native.systemFont,
     width = 40,
     height = 40,
     cornerRadius = 4,
     labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
     fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
     strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
     strokeWidth = 4,
     onEvent = openOptions
   }
   sceneGroup:insert(optionsButton)

   opti = display.newImageRect("GFX/Common/pauseButton.png", 50, 50)
   opti.x = optionsButton.x
   opti.y = optionsButton.y
   sceneGroup:insert(opti)

   --Create check button so long as we aren't on level 4 (plot disposition).
   if(nextNum ~= 5) then
     checkButton = widget.newButton{
       left = display.contentCenterX-50,
       top = display.contentHeight-26,
       label = "Submit",
       shape = "roundedRect",
       font = native.systemFont,
       width = 100,
       height = 25,
       cornerRadius = 4,
       labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
       fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
       strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
       strokeWidth = 4,
       onEvent = checkAnswer
     }
     sceneGroup:insert(checkButton)
   end

   resetNumberOfButtons()

  initializeSidePanel(sceneGroup)

   --Build levels.
   if (nextNum == 2) then
     level1Build()
     createSpawnLocation()
     sceneGroup:insert(spawn:getButton())
   elseif (nextNum == 3) then
     level2Build()
     createSpawnLocation()
     sceneGroup:insert(spawn:getButton())
   elseif (nextNum == 4) then
     level3Build()
     createSpawnLocation()
     sceneGroup:insert(spawn:getButton())
   elseif (nextNum == 5) then

          initializeSidePanel(sceneGroup)
   else
     --U dun goofed if you see this.
     print("Error in building level. (Game.scene:create())")
   end

   --TODO Clean these comments up.
   --add logic button
  -- addToggleButton(event, 100, 50, 42,42,logicButtonValues,logicButtonGroup,logicImages,logicButtonPress)
   --add true/false button
  -- addToggleButton(event, 200, 50, 60,42,trueFalseButtonValues,trueFalseButtonGroup,trueFalseImages,trueFalseButtonPress)
   --add logic buttons to scene
   addGroupToScene(event,logicButtonGroup, sceneGroup)
   --add true false buttons to scene
   addGroupToScene(event,trueFalseButtonGroup, sceneGroup)
   addClassGroupToScene(event,locations,sceneGroup)

end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
    -- audio.stop(1)
     --audio.rewind(bgmusic)
   elseif ( phase == "did" ) then
     --TODO: Let's optimize this slightly. If we move everything to scene:show we may not need to delete it everytime.
     composer.removeScene("Code.Game1.nextLevel")
      --If it's not level 4 (plot disposition) add check answer listener.

      audio.play(bgmusic, {channel=1,loops=-1})
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      removeGroupFromScene(event,logicButtonGroup)
      removeGroupFromScene(event,trueFalseButtonGroup)
      --audio.stop(1)
   elseif ( phase == "did" ) then
     --Remove listeners so stuff doesn't bug out.

      sideTabButton:removeEventListener("touch",openSideTab)

      removeListenerFromGroup(event, logicButtonGroup, logicButtonPress)
      removeListenerFromGroup(event, trueFalseButtonGroup, trueFalseButtonPress)
      composer.removeScene("Code.Game1.game")
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view
     --Clean up
     gamebg:removeSelf()
     gamebg = nil
     sideTabButton:removeSelf()
     sideTabButton = nil

     --audio.dispose(bgmusic)
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
