local composer = require( "composer" )
local scene = composer.newScene()
local widget = require("widget")
---------------------------------------------------------------------------------

-- local forward references should go here
local background
local next
local back
local text
local backi
local tut1
local tut2
local tut3
local tut4
local tut5
local soundEffect = audio.loadSound( "SFX/Common/Menu1.wav" )

---------------------------------------------------------------------------------

--Return to the title screen.
local function toTitle(event)
  --Create appropriate options and parameters
  local options = {
    effect = "fade",
    time = 1000
  }
  --Go to title.lua
  if event.phase == "ended" then
    audio.play(soundEffect, {channel=2})
    composer.gotoScene("Code.Game1.title", options)
  end
end

local function cycleTut(event)
  if event.phase == "ended" then
    if event.target.id == 1 then
      text.isVisible = false
      tut1.isVisible = false
      tut2.isVisible = true
    elseif event.target.id == 2 then
      tut2.isVisible = false
      tut3.isVisible = true
    elseif event.target.id == 3 then
      tut3.isVisible = false
      tut4.isVisible = true
    elseif event.target.id == 4 then
      tut4.isVisible = false
      tut5.isVisible = true
    else
      toTitle(event)
    end
  end
end
-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view

   --Create and set background image.
   background = display.newImageRect("GFX/Common/title.png", 480, 320)
   background.anchorX = 0
   background.anchorY = 0
   background.x = 0
   background.y = 0
   sceneGroup:insert(background)



    tut1 = display.newImageRect("GFX/Game1/tut/tut1.png", 480, 320)
    tut1.x = display.contentCenterX
    tut1.y = display.contentCenterY
    tut1.id = 1
    sceneGroup:insert(tut1)

    tut2 = display.newImageRect("GFX/Game1/tut/tut2.png", 480, 320)
    tut2.x = display.contentCenterX
    tut2.y = display.contentCenterY
    tut2.isVisible = false
    tut2.id = 2
    sceneGroup:insert(tut2)

    tut3 = display.newImageRect("GFX/Game1/tut/tut3.png", 480, 320)
    tut3.x = display.contentCenterX
    tut3.y = display.contentCenterY
    tut3.isVisible = false
    tut3.id = 3
    sceneGroup:insert(tut3)

    tut4 = display.newImageRect("GFX/Game1/tut/tut4.png", 480, 320)
    tut4.x = display.contentCenterX
    tut4.y = display.contentCenterY
    tut4.isVisible = false
    tut4.id = 4
    sceneGroup:insert(tut4)

    tut5 = display.newImageRect("GFX/Game1/tut/tut5.png", 480, 320)
    tut5.x = display.contentCenterX
    tut5.y = display.contentCenterY
    tut5.isVisible = false
    tut5.id = 5
    sceneGroup:insert(tut5)

    text = display.newText(sceneGroup, "Tap anywhere to continue!", display.contentWidth - 100, 10, native.systemFontBold, 16)
    text:setTextColor(1, 0, 0)

    --Create and set back button image.

     backButton = widget.newButton{
       left = display.contentWidth - 43,
       top = display.contentHeight - 43,
       shape = "roundedRect",
       font = native.systemFont,
       width = 40,
       height = 40,
       cornerRadius = 4,
       labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
       fillColor = { default={ 0.5, 0.60, 0.5, 1 }, over={ 0.38, 0.27, 0.32, 1 } },
       strokeColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
       strokeWidth = 4,
       onEvent = toTitle
     }
     sceneGroup:insert(backButton)

     backi = display.newImageRect("GFX/Common/left224.png", 35, 35)
     backi.x = backButton.x
     backi.y = backButton.y
     sceneGroup:insert(backi)
end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
      -- Called when the scene is still off screen (but is about to come on screen).

   elseif ( phase == "did" ) then
     --Add appropriate listener.
     tut1:addEventListener("touch", cycleTut)
     tut2:addEventListener("touch", cycleTut)
     tut3:addEventListener("touch", cycleTut)
     tut4:addEventListener("touch", cycleTut)
     tut5:addEventListener("touch", cycleTut)
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
     --Remove listeners so nothing bugs out.
     tut1:removeEventListener("touch", cycleTut)
     tut2:removeEventListener("touch", cycleTut)
     tut3:removeEventListener("touch", cycleTut)
     tut4:removeEventListener("touch", cycleTut)
     tut5:removeEventListener("touch", cycleTut)
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

   --Clean up. Let's sing along.
   background:removeSelf()
   background = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
