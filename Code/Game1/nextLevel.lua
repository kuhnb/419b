local composer = require( "composer" )
local scene = composer.newScene()

---------------------------------------------------------------------------------

-- local forward references should go here
  local nextLevel
  local curNum

---------------------------------------------------------------------------------

-- "scene:create()"
function scene:create( event )

   local sceneGroup = self.view
   curNum = event.params.num
   nextLevel = event.params.level
   print("Transitioning to level: " .. (curNum-1))

end

-- "scene:show()"
function scene:show( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
     --Remove this scene so everything is reinitialized properly. TODO: Optimize this.
     composer.removeScene("Code.Game1.game")
      --Generate appropraite options and parameters
      local options = {
        effect = "fade",
        time = 500,
        params = {
          num = curNum,
          level = nextLevel
        }
      }
      --Go to title.lua if all 4 levels are complete
      if curNum == 5 then
        audio.stop(1)
        composer.gotoScene("Code.Game1.title", {effect = "fade", time = 1000})
      --Else go to game.lua
      else
        composer.gotoScene("Code.Game1.game", options)
      end
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then

   elseif ( phase == "did" ) then
    --We want to call the scene:create again at a later time.
    composer.removeScene("Code.Game1.nextLevel")
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view
   --Clean up.
   curNum = nil
   nextLevel = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
